namespace plugin
{
    public class VersionData
    {
        public string Version;
    }

    public class HullData
    {
        public string ClassName;
        public string SaveKey;
        public string Faction;
        public string HullClassification;
        public string LongDescription;
        public string FlavorText;
        public string SizeClass;
        public int PointCost;
        public float Mass;
        public float MaxSpeed;
        public float LinearMotor;
        public float MaxTurnSpeed;
        public float AngularMotor;
        public float BaseIntegrity;
        public float ArmorThickness;
        public float InteriorDensityArmorEquivalent;
        public float MaxComponentDR;
        public float CrewVulnerability;
        public float IdentityWorkValue;
        public int BaseCrewComplement;
        public int VisionDistance;
        public string EditorFormatHullBuffs;
        public SignatureData[] SignatureData;
        public MeshData[] MeshData;
        public float BaseSigRadius;
    }

    public class SignatureData
    {
        public float x;
        public float y;
        public float z;
        public int[] neighbors;
        public float value;
    }

    public class MeshData
    {
        public MeshVerticesData[] MeshVerticesData;
        public int[] MeshTriangleData;
    }

    public class MeshVerticesData
    {
        public float x;
        public float y;
        public float z;
    }

    public class ComponentData
    {
        public string ComponentName;
        public string SaveKey;
        public string Faction;
        public string ShortDescription;
        public string LongDescription;
        public string FlavorText;
        public string Category;
        public string Type;
        public bool CompoundingCost;
        public float CompoundingMultiplier;
        public string CompoundingCostClass;
        public bool FirstInstanceFree;
        public int PointCost;
        public float Mass;
        public Vector3IntData Size;
        public bool CanTile;
        public float MaxHealth;
        public bool Reinforced;
        public float FunctioningThreshold;
        public float DamageThreshold;
        public string DCPriority;
        public string RareDebuffSubtype;
        public float RareDebuffChance;
        public string FormattedResources;
        public string ResourceDemandPriority;
        public string FormattedBuffs;

        public IWeaponData IWeaponData;
        public WeaponComponentData WeaponComponentData;
        public ContinuousWeaponComponentData ContinuousWeaponComponentData;
        public DiscreteWeaponComponentData DiscreteWeaponComponentData;
        public CellLauncherComponentData CellLauncherComponentData;
        public TubeLauncherComponentData TubeLauncherComponentData;
        public FixedContinuousWeaponComponentData FixedContinuousWeaponComponentData;

        public MuzzleData MuzzleData;

        public DamageCharacteristicData RaycastDamageCharacteristicData;
        public DamageFalloffData[] RaycastDamageFalloffData;

        public CommsAntennaComponentData CommsAntennaComponentData;

        public OmnidirectionalEWarComponentData OmnidirectionalEWarComponentData;
        public TurretedEWarComponentData TurretedEWarComponentData;

        public ISensorComponentData ISensorComponentData;
        public PassiveSensorComponentData PassiveSensorComponentData;
        public SensorComponentData SensorComponentData;
        public FireControlData FireControlData;

        public IEWarTargetData IEWarTargetData;

        public SensorTurretComponentData SensorTurretComponentData;
        public TurretedContinuousWeaponComponentData TurretedContinuousWeaponComponentData;
        public TurretedDiscreteWeaponComponentData TurretedDiscreteWeaponComponentData;

        public IIntelComponentData IIntelComponentData;
        public CommandComponentData CommandComponentData;
        public IntelligenceComponentData IntelligenceComponentData;

        public BerthingComponentData BerthingComponentData;
        public CrewOperatedComponentData CrewOperatedComponentData;

        public DCLockerComponentData DCLockerComponentData;

        public IMagazineProviderData IMagazineProviderData;
        public BulkMagazineComponentData BulkMagazineComponentData;
    }

    public class Vector3IntData
    {
        public int x;
        public int y;
        public int z;
    }

    public class BerthingComponentData
    {
        public int CrewProvided;
    }

    public class BulkMagazineComponentData
    {
        public int AvailableVolume;
    }

    public class CellLauncherComponentData
    {
        public float TimeBetweenCells;
        public int MissileSize;
    }

    public class CommandComponentData
    {
        public float IntelEffort;
        public float IntelAccuracy;
    }

    public class CommsAntennaComponentData
    {
        public float TransmitPower;
        public int Bandwidth;
        public float Gain;
    }

    public class ContinuousWeaponComponentData
    {
        public float BurstDuration;
        public float CooldownTime;
        public string CooldownStyle;
        public float GradualCooldownSpeed;
        public bool BattleshortAvailable;
        public float OverheatDamageProbability;
        public float OverheatDamagePerSecond;
        public bool SingleCycleOrder;
    }

    public class CrewOperatedComponentData
    {
        public int CrewRequired;
        public int ReinforceCrewAt;
    }

    public class DCLockerComponentData
    {
        public int TeamsProduced;
        public float TeamRepairRate;
        public float MovementSpeed;
        public bool CanRestore;
        public int RestoreCount;
    }

    public class DiscreteWeaponComponentData
    {
        public float TimeBetweenMuzzles;
        public int MagazineSize;
        public float ReloadTime;
    }

    public class FixedContinuousWeaponComponentData
    {
        public bool SteerableBeam;
        public float SteerAngle;
        public float SteerRate;
    }

    public class IntelligenceComponentData
    {
        public float IntelEffort;
        public float IntelAccuracy;
    }

    public class OmnidirectionalEWarComponentData
    {
        public string SigType;
        public float MaxRange;
        public float EffectAreaRatio;
        public float RadiatedPower;
        public float Gain;
    }

    public class PassiveSensorComponentData
    {
        public float Accuracy;
    }

    public class SensorComponentData
    {
        public string SigType;
        public float MaxRange;
        public float RadiatedPower;
        public float Gain;
        public float Sensitivity;
        public float ApertureSize;
        public float NoiseFiltering;
        public bool CanLock;
        public float LockErrorMultiplier;
        public float MaintainLockSNR;
        public float BurnthroughPowerMultiplier;
        public float BurnthroughDamageProbability;
        public float BurnthroughDamage;
        public RadiationData[] RadiationData;
        public DeviationData[] DeviationData;
    }

    public class FireControlData
    {
        public string SigType;
        public float MaxRange;
        public float RadiatedPower;
        public float Gain;
        public float ApertureSize;
        public float NoiseFiltering;
        public float LockErrorMultiplier;
        public float MaintainLockSNR;
        public RadiationData[] RadiationData;
        public DeviationData[] DeviationData;
    }
    public class RadiationData
    {
        public float Direction;
        public float Value;
    }
    public class DeviationData
    {
        public float SignalLoss;
        public float DeviationMultiplier;
    }

    public class SensorTurretComponentData
    {
        public float TraverseRate;
        public float ElevationRate;
        public float MaxElevation;
        public float MinElevation;
    }

    public class TubeLauncherComponentData
    {
        public float TimeBetweenLaunches;
        public int MissileSize;
        public int LaunchesPerLoad;
        public float LoadTime;
    }

    public class TurretedContinuousWeaponComponentData
    {
        public float TraverseRate;
        public float ElevationRate;
        public float MaxElevation;
        public float MinElevation;
        public bool LimitRotationSpeedWhenFiring;
        public float FiringRotationSpeedModifier;
    }

    public class TurretedDiscreteWeaponComponentData
    {
        public float TraverseRate;
        public float ElevationRate;
        public float MaxElevation;
        public float MinElevation;
    }

    public class TurretedEWarComponentData
    {
        public float MaxRange;
        public float EffectAreaRatio;
        public float RadiatedPower;
        public float Gain;
        public float ConeFov;
    }

    public class WeaponComponentData
    {
        public string OptimalTargetWeight;
        public bool AllowGrouping;
        public string GroupingName;
        public string Role;
        public string EwType;
        public bool CheckFriendlyFire;
        public bool CheckObstaclesInWay;
        public bool RequireExternalAmmoFeed;
        public bool AllowAmmoSelection;
        public bool LeadTargets;
        public float OnTargetAngle;
    }

    public class MuzzleData
    {
        public float? MaxRange;
        public string SimMethod;
        public bool IgnoreAccuracyModifiers;
        public float BaseAccuracy;
        public AccuracyData[] AccuracyData;

        public float damagePeriod;
        public float damagePerSecond;
        public float armorPerSecond;
    }

    public class AccuracyData
    {
        public float Range;
        public float Spread;
    }

    public class DamageFalloffData
    {
        public float Range;
        public float DamageModifier;
    }

    public class IEWarTargetData
    {
        public string SigType;
    }

    public class IIntelComponentData
    {
        public bool WorkOnRemoteTracks;
    }

    public class IMagazineProviderData
    {
        public bool VolumeBased;
        public bool CanFeedExternally;
        public bool CanProvide;
        public string ProviderKey;
    }

    public class ISensorComponentData
    {
        public bool IgnoreEMCON;
        public bool CanOrderLock;
        public bool CanOrderBurnthrough;
    }

    public class IWeaponData
    {
        public string WepType;
        public string OptimalTargetWeight;
        public string PDTTargetMethod;
        public bool NeedsExternalAmmoFeed;
        public bool SupportsAmmoSelection;
        public bool? SupportsPositionTargeting;
        public bool? PositionTargetingNoInput;
        public bool? SupportsTrackTargeting;
        public bool? SupportsVisualTargeting;
        public bool? SupportsWaypoints;
    }

    public class MunitionData
    {
        public string MunitionName;
        public string SaveKey;
        public string Description;
        public string Type;
        public string SimMethod;
        public string Role;
        public int PointCost;
        public int PointDivision;
        public float StorageVolume;
        public bool SupportsPositionTargeting;
        public bool PositionTargetingNoInput;
        public bool SupportsTrackTargeting;
        public bool SupportsVisualTargeting;
        public bool SupportsWaypoints;
        public float FlightSpeed;
        public float Lifetime;
        public float MaxRange;
        public LWMunitionData LWMunitionData;
        public MissileData MissileData;
        public DamageCharacteristicData DamageCharacteristics;
        
        public string FlavorText;

        public float PenetrationEnvelope;
        public float ExplosionRadius;


    }

    public class LWMunitionData
    {
        public float MaxPenetration;
        //public PenetrationFalloffData[] PenetrationFalloffData;
    }

    public class PenetrationFalloffData
    {
        public float Range;
        public float Penetration;
    }

    public class MissileData
    {
        public int Size;
        public SignatureData[] SignatureData;
    }

    public class DamageCharacteristicData
    {
        public float ArmorPenetration;
        public float OverpenetrationDamageMultiplier;
        public float? MaxPenetrationDepth;
        public float HeatDamage;
        public float DamageBrushSize;
        public float ComponentDamage;
        public float RandomEffectMultiplier;
        public float CrewVulnerabilityMultiplier;
        public bool IgnoreEffectiveThickness;
        public bool NeverRicochet;
    }

   
}