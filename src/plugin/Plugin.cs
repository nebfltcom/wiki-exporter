﻿using BepInEx;
using Munitions;
using Newtonsoft.Json;
using Ships;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;
using UniverseLib.Runtime;
using Utility;

namespace plugin
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [BepInProcess("Nebulous.exe")]
    public class Plugin : BaseUnityPlugin
    {
        private int _phase = 0;
        private int _faction = 0;
        private float _lastUpdate = 0;
        private float _updateInterval = 5.0f;
        private string _filepath;
        private void Awake()
        {
            // Plugin startup logic
            Logger.LogInfo($"Wiki Exporter plugin loaded!");
            UniverseLib.Universe.Init();
            StartCoroutine(GetFilePath("http://localhost:9997/filepath", this.SaveFilePath));
        }
        private async void Update()
        {
            if (this._lastUpdate + this._updateInterval < Time.time)
            {
                this._lastUpdate = Time.time;
                if (this._phase == 0)
                {
                    //Check for main menu
                    var foundMainMenu = FindObjectsOfType<UI.MainMenu>();
                    if (foundMainMenu.Length > 0)
                    {
                        Logger.LogInfo($"Found Main Menu, game is loaded");
                        var mainMenu = foundMainMenu[0];
                        mainMenu.FleetEditorButton();
                        this._phase++;
                    }
                }
                else if (this._phase == 1)
                {
                    //Get file path for saving images and send game version
                    if (this._filepath != null)
                    {
                        Logger.LogInfo($"File path is {this._filepath}");
                        this._phase++;
                    }

                    var versionData = new VersionData();
                    versionData.Version = Application.version;
                    var json = JsonConvert.SerializeObject(versionData);
                    Logger.LogInfo($"Serialized JSON for game version data");

                    UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/version", json);
                    webRequest.SetRequestHeader("Content-Type", "application/json");
                    webRequest.SendWebRequest();
                }
                else if (this._phase == 2)
                {
                    //Choosing Faction
                    var foundModalListSelectDetailed = FindObjectsOfType<UI.ModalListSelectDetailed>();
                    if (foundModalListSelectDetailed.Length > 0)
                    {
                        Logger.LogInfo($"Found ModalListSelectDetailed, choosing faction");
                        var factionListSelector = foundModalListSelectDetailed[0];
                        var _items = (List<UI.SelectableListItem>)typeof(UI.ModalListSelect).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(factionListSelector);
                        factionListSelector.SelectItem(_items[this._faction], true);
                        var allFactions = Bundles.BundleManager.Instance.AllFactions.ToArray();
                        Logger.LogInfo($"Selected {allFactions[this._faction].FactionName}");
                        this._phase++;
                    }
                }
                else if (this._phase == 3)
                {
                    //Open Create Ship modal
                    var foundFleetEditorController = FindObjectsOfType<FleetEditor.FleetEditorController>();
                    var allFactions = Bundles.BundleManager.Instance.AllFactions.ToArray();
                    if (foundFleetEditorController.Length > 0)
                    {
                        Logger.LogInfo($"Found FleetEditorController, creating ships");
                        var fleetCompController = foundFleetEditorController[0];
                        foreach (var currHull in Bundles.BundleManager.Instance.AllHulls)
                        {
                            //Skip Raines or Tugboat, since its created by default
                            if (currHull.ClassName != "Raines" && currHull.ClassName != "Tugboat" && currHull.FactionKey == allFactions[this._faction].SaveKey)
                            {
                                fleetCompController.CreateNewShip(currHull);
                                Logger.LogInfo($"Created {currHull.ClassName}");
                            
                            }
                        }
                        this._phase++;
                    }
                }
                else if (this._phase == 4)
                {
                    //Start sending hull data
                    var foundShips = FindObjectsOfType<Ship>();
                    if (foundShips.Length > 0)
                    {
                        foreach (var currShip in foundShips)
                        {
                            var currHull = currShip.Hull;
                            if (currHull is Ships.BaseHull)
                            {
                                Logger.LogInfo($"Found hull {currHull.ClassName}");
                                var silhouettePath = this._filepath + "\\" + this.FormatName(currHull.ClassName) + "_silhouette.png";
                                var silhouetteTexture = currHull.Silhouette != null ? currHull.Silhouette.texture : currHull.SmallSilhouette.texture;
                                if (!TextureHelper.IsReadable(silhouetteTexture))
                                {
                                    silhouetteTexture = TextureHelper.ForceReadTexture(silhouetteTexture);
                                }
                                byte[] silhouetteData = TextureHelper.EncodeToPNG(silhouetteTexture);
                                File.WriteAllBytes(silhouettePath, silhouetteData);
                                Logger.LogInfo($"Saving {currHull.ClassName} silhouette to {silhouettePath}");

                                var screenshotPath = this._filepath + "\\" + this.FormatName(currHull.ClassName) + "_screenshot.png";
                                var screenshotTexture = currHull.HullScreenshot.texture;
                                if (!TextureHelper.IsReadable(screenshotTexture))
                                {
                                    screenshotTexture = TextureHelper.ForceReadTexture(screenshotTexture);
                                }
                                byte[] screenshotData = TextureHelper.EncodeToPNG(screenshotTexture);
                                File.WriteAllBytes(screenshotPath, screenshotData);
                                Logger.LogInfo($"Saving {currHull.ClassName} screenshot to {screenshotPath}");

                                //Send JSON data
                                var hullData = new HullData();
                                hullData.ClassName = currHull.ClassName;
                                hullData.SaveKey = currHull.SaveKey;
                                hullData.Faction = currHull.FactionKey;
                                hullData.HullClassification = currHull.HullClassification;
                                hullData.LongDescription = currHull.LongDescription;
                                hullData.FlavorText = currHull.FlavorText;
                                hullData.PointCost = currHull.PointCost;
                                hullData.SizeClass = currHull.SizeClass.ToString();
                                hullData.BaseCrewComplement = currHull.BaseCrewComplement;
                                hullData.Mass = currHull.Mass;
                                hullData.MaxSpeed = currHull.MaxSpeed * 10;
                                hullData.MaxTurnSpeed = currHull.MaxTurnSpeed * 57.29578f;
                                hullData.LinearMotor = currHull.LinearMotor / 100;
                                hullData.AngularMotor = currHull.AngularMotor / 100;
                                var _radarSignature = (Game.Sensors.Signature)typeof(Ships.BaseHull).GetField("_radarSignature", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currHull);
                                hullData.CrewVulnerability = currHull.CrewVulnerability;
                                hullData.ArmorThickness = currHull.ArmorThickness;
                                hullData.InteriorDensityArmorEquivalent = (float)typeof(Ships.BaseHull).GetField("_interiorDensityArmorEquivalent", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currHull);
                                hullData.MaxComponentDR = currHull.MaxComponentDR;
                                hullData.VisionDistance = currHull.VisionDistance * 500;
                                var _structure = currHull._structure;
                                hullData.BaseIntegrity = (float)typeof(Ships.HullStructure).GetField("_baseIntegrity", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_structure);
                                hullData.IdentityWorkValue = currHull.IdentityWorkValue;
                                hullData.EditorFormatHullBuffs = currHull.EditorFormatHullBuffs();
                                var _samplePositions = (Vector3[])typeof(Game.Sensors.CrossSection).GetField("_samplePositions", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(null);
                                var _neighbors = (int[,])typeof(Game.Sensors.CrossSection).GetField("_neighbors", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(null);
                                hullData.SignatureData = new SignatureData[162];
                                for (int i = 0; i <= 161; i++)
                                {
                                    var currSignatureData = new SignatureData();
                                    currSignatureData.x = _samplePositions[i].x;
                                    currSignatureData.y = _samplePositions[i].y;
                                    currSignatureData.z = _samplePositions[i].z;
                                    currSignatureData.neighbors = new int[6];
                                    for (int j = 0; j < 6; j++)
                                    {
                                        currSignatureData.neighbors[j] = _neighbors[i, j];
                                    }
                                    currSignatureData.value = _radarSignature.GetCrossSection(-_samplePositions[i]) * 10;
                                    hullData.SignatureData[i] = currSignatureData;
                                }
                                currHull.SpawnColliderSampler();
                                var _colliderSampler = (Ships.BaseColliderSampler)typeof(Ships.BaseHull).GetField("_colliderSampler", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currHull);
                                var _colliders = (MeshCollider[])typeof(Ships.BaseColliderSampler).GetProperty("_allColliders", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_colliderSampler);
                                var meshData = new List<MeshData>();
                                for (int i = 0; i < _colliders.Length; i++)
                                {
                                    var currMesh = _colliders[i].sharedMesh;
                                    if (currMesh != null)
                                    {
                                        var currMeshData = new MeshData();
                                        currMeshData.MeshVerticesData = new MeshVerticesData[currMesh.vertices.Length];
                                        for (int j = 0; j < currMeshData.MeshVerticesData.Length; j++)
                                        {
                                            var currMeshVerticesData = new MeshVerticesData();
                                            var realVector = currMesh.vertices[j] + Vector3.zero;
                                            realVector += _colliders[i].transform.parent.transform.localPosition;
                                            currMeshVerticesData.x = realVector.x;
                                            currMeshVerticesData.y = realVector.y;
                                            currMeshVerticesData.z = realVector.z;
                                            currMeshData.MeshVerticesData[j] = currMeshVerticesData;
                                        }
                                        currMeshData.MeshTriangleData = currMesh.triangles;
                                        meshData.Add(currMeshData);
                                    }
                                }
                                hullData.MeshData = meshData.ToArray();

                                hullData.BaseSigRadius = _radarSignature.SigRadius * 10;


                                var json = JsonConvert.SerializeObject(hullData);
                                Logger.LogInfo($"Serialized JSON for {currHull.ClassName}: {json}");

                                UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/hull", json);
                                webRequest.SetRequestHeader("Content-Type", "application/json");
                                webRequest.SendWebRequest();
                            }
                        }
                        this._phase++;
                    }
                }
                else if (this._phase == 5)
                {
                    //Return to phase 2 if factions still left
                    if (this._faction < Bundles.BundleManager.Instance.AllFactions.Count - 1)
                    {
                        var foundMainMenu = FindObjectsOfType<UI.MainMenu>();
                        var foundFleetEditorController = FindObjectsOfType<FleetEditor.FleetEditorController>();
                        if (foundFleetEditorController.Length > 0)
                        {
                            var fleetCompController = foundFleetEditorController[0];
                            typeof(FleetEditor.FleetEditorController).GetMethod("ExitInternal", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(fleetCompController, new object[] { });
                        }
                        else if (foundMainMenu.Length > 0)
                        {
                            var mainMenu = foundMainMenu[0];
                            mainMenu.FleetEditorButton();
                            this._faction++;
                            this._phase = 2;
                        }
                    }
                    else
                    {
                        this._phase++;
                    }
                }
                else if (this._phase == 6)
                {
                    //Start sending component data
                    foreach (var currComponent in Bundles.BundleManager.Instance.AllComponents)
                    {
                        Logger.LogInfo($"Found component {currComponent.ComponentName}");
                        try
                        {
                            var smallImagePath = this._filepath + "\\" + this.FormatName(currComponent.ComponentName) + "_smallimage.png";
                            var smallImageTexture = currComponent.SmallImage.texture;
                            if (!TextureHelper.IsReadable(smallImageTexture))
                            {
                                smallImageTexture = TextureHelper.ForceReadTexture(smallImageTexture);
                            }
                            byte[] smallImageData = TextureHelper.EncodeToPNG(smallImageTexture);
                            File.WriteAllBytes(smallImagePath, smallImageData);
                            Logger.LogInfo($"Saving {currComponent.ComponentName} small image to {smallImagePath}");
                        }
                        catch (NullReferenceException e)
                        {
                            Logger.LogInfo($"Small image not found, skipping {currComponent.ComponentName}");
                        }

                        try
                        {
                            var largeImagePath = this._filepath + "\\" + this.FormatName(currComponent.ComponentName) + "_largeimage.png";
                            var largeImageTexture = currComponent.LargeImage.texture;
                            if (!TextureHelper.IsReadable(largeImageTexture))
                            {
                                largeImageTexture = TextureHelper.ForceReadTexture(largeImageTexture);
                            }
                            byte[] largeImageData = TextureHelper.EncodeToPNG(largeImageTexture);
                            File.WriteAllBytes(largeImagePath, largeImageData);
                            Logger.LogInfo($"Saving {currComponent.ComponentName} large image to {largeImagePath}");
                        }
                        catch (NullReferenceException e)
                        {
                            Logger.LogInfo($"Large image not found, skipping {currComponent.ComponentName}");
                        }

                        //Send JSON data
                        var componentData = new ComponentData();
                        componentData.ComponentName = currComponent.ComponentName;
                        componentData.SaveKey = currComponent.SaveKey;
                        var _factionKey = (string)typeof(Ships.HullComponent).GetField("_factionKey", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        componentData.Faction = _factionKey;
                        var _shortDescription = (string)typeof(Ships.HullComponent).GetField("_shortDescription", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        componentData.ShortDescription = _shortDescription;
                        var _longDescription = (string)typeof(Ships.HullComponent).GetField("_longDescription", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        componentData.LongDescription = _longDescription;
                        componentData.FlavorText = currComponent.FlavorText;
                        componentData.Category = currComponent.Category;
                        componentData.CompoundingCost = currComponent.CompoundingCost;
                        var _compoundingMultiplier = (float)typeof(Ships.HullComponent).GetField("_compoundingMultiplier", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        componentData.CompoundingMultiplier = _compoundingMultiplier;
                        componentData.CompoundingCostClass = currComponent.CompoundingCostClass;
                        componentData.FirstInstanceFree = currComponent.FirstInstanceFree;
                        componentData.PointCost = currComponent.PointCost;
                        componentData.Type = currComponent.Type.ToString();
                        var sizeData = new Vector3IntData();
                        sizeData.x = currComponent.Size.x;
                        sizeData.y = currComponent.Size.y;
                        sizeData.z = currComponent.Size.z;
                        componentData.Size = sizeData;
                        componentData.Mass = currComponent.Mass;
                        componentData.CanTile = currComponent.CanTile;
                        componentData.DCPriority = currComponent.DCPriority.ToString();
                        componentData.FormattedResources = currComponent.GetFormattedResources();
                        componentData.ResourceDemandPriority = currComponent.ResourceDemandPriority.ToString();
                        componentData.FormattedBuffs = currComponent.GetFormattedBuffs();
                        var _maxHealth = (float)typeof(Ships.HullPart).GetField("_maxHealth", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        var _functioningThreshold = (float)typeof(Ships.HullPart).GetField("_functioningThreshold", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        var _damageThreshold = (float)typeof(Ships.HullPart).GetField("_damageThreshold", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        var _reinforced = (bool)typeof(Ships.HullPart).GetField("_reinforced", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        var _rareDebuffChance = (float)typeof(Ships.HullComponent).GetField("_rareDebuffChance", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        var _rareDebuffSubtype = (string)typeof(Ships.HullComponent).GetField("_rareDebuffSubtype", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        componentData.MaxHealth = _maxHealth;
                        componentData.FunctioningThreshold = _functioningThreshold;
                        componentData.DamageThreshold = _damageThreshold;
                        componentData.Reinforced = _reinforced;
                        componentData.RareDebuffChance = _rareDebuffChance;
                        componentData.RareDebuffSubtype = _rareDebuffSubtype;

                        if (currComponent is Ships.BerthingComponent)
                        {
                            Logger.LogInfo($"Grabbing BerthingComponentData Info");
                            var berthingComponentData = new BerthingComponentData();
                            var _crewProvided = (int)typeof(Ships.BerthingComponent).GetField("_crewProvided", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            berthingComponentData.CrewProvided = _crewProvided;
                            componentData.BerthingComponentData = berthingComponentData;
                        }

                        if (currComponent is Ships.BulkMagazineComponent)
                        {
                            Logger.LogInfo($"Grabbing BulkMagazineComponent Info");
                            var bulkMagazineComponentData = new BulkMagazineComponentData();
                            var _availableVolume = (int)typeof(Ships.BulkMagazineComponent).GetField("_availableVolume", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            bulkMagazineComponentData.AvailableVolume = _availableVolume;
                            componentData.BulkMagazineComponentData = bulkMagazineComponentData;
                        }

                        if (currComponent is Ships.BaseCellLauncherComponent)
                        {
                            Logger.LogInfo($"Grabbing CellLauncherComponent Info");
                            var cellLauncherComponentData = new CellLauncherComponentData();
                            var _timeBetweenCells = (float)typeof(Ships.BaseCellLauncherComponent).GetField("_timeBetweenCells", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _missileSize = (int)typeof(Ships.BaseCellLauncherComponent).GetField("_missileSize", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            cellLauncherComponentData.TimeBetweenCells = _timeBetweenCells;
                            cellLauncherComponentData.MissileSize = _missileSize;
                            componentData.CellLauncherComponentData = cellLauncherComponentData;
                        }

                        if (currComponent is Ships.CommandComponent)
                        {
                            Logger.LogInfo($"Grabbing CommandComponent Info");
                            var commandComponentData = new CommandComponentData();
                            var _intelEffort = (float)typeof(Ships.CommandComponent).GetField("_intelEffort", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _intelAccuracy = (float)typeof(Ships.CommandComponent).GetField("_intelAccuracy", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            commandComponentData.IntelEffort = _intelEffort;
                            commandComponentData.IntelAccuracy = _intelAccuracy;
                            componentData.CommandComponentData = commandComponentData;
                        }

                        if (currComponent is Ships.CommsAntennaComponent)
                        {
                            Logger.LogInfo($"Grabbing CommsAntennaComponent Info");
                            var commsAntennaComponentData = new CommsAntennaComponentData();
                            var _transmitPower = (float)typeof(Ships.CommsAntennaComponent).GetField("_transmitPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _bandwidth = (int)typeof(Ships.CommsAntennaComponent).GetField("_bandwidth", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _gain = (float)typeof(Ships.CommsAntennaComponent).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            commsAntennaComponentData.TransmitPower = _transmitPower;
                            commsAntennaComponentData.Bandwidth = _bandwidth;
                            commsAntennaComponentData.Gain = _gain;
                            componentData.CommsAntennaComponentData = commsAntennaComponentData;
                        }

                        if (currComponent is Ships.ContinuousWeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing ContinuousWeaponComponent Info");
                            var continuousWeaponComponentData = new ContinuousWeaponComponentData();
                            var _burstDuration = (float)typeof(Ships.ContinuousWeaponComponent).GetField("_burstDuration", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _cooldownTime = (float)typeof(Ships.ContinuousWeaponComponent).GetField("_cooldownTime", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _gradualCooldownSpeed = (float)typeof(Ships.ContinuousWeaponComponent).GetField("_gradualCooldownSpeed", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _battleShortIgnoreCooldown = (bool)typeof(Ships.ContinuousWeaponComponent).GetField("_battleShortIgnoreCooldown", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _overheatDamageProbability = (float)typeof(Ships.ContinuousWeaponComponent).GetField("_overheatDamageProbability", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _overheatDamagePerSecond = (float)typeof(Ships.ContinuousWeaponComponent).GetField("_overheatDamagePerSecond", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _singleCycleOrder = (bool)typeof(Ships.ContinuousWeaponComponent).GetField("_singleCycleOrder", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _cooldownStyle = (Ships.ContinuousWeaponComponent.CooldownType)typeof(Ships.ContinuousWeaponComponent).GetField("_cooldownStyle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            continuousWeaponComponentData.BurstDuration = _burstDuration;
                            continuousWeaponComponentData.CooldownTime = _cooldownTime;
                            continuousWeaponComponentData.GradualCooldownSpeed = _gradualCooldownSpeed;
                            continuousWeaponComponentData.BattleshortAvailable = _battleShortIgnoreCooldown;
                            continuousWeaponComponentData.OverheatDamageProbability = _overheatDamageProbability;
                            continuousWeaponComponentData.OverheatDamagePerSecond = _overheatDamagePerSecond;
                            continuousWeaponComponentData.SingleCycleOrder = _singleCycleOrder;
                            continuousWeaponComponentData.CooldownStyle = _cooldownStyle.ToString();
                            componentData.ContinuousWeaponComponentData = continuousWeaponComponentData;
                        }

                        if (currComponent is Ships.CrewOperatedComponent)
                        {
                            Logger.LogInfo($"Grabbing CrewOperatedComponent Info");
                            var crewOperatedComponentData = new CrewOperatedComponentData();
                            var _crewRequired = (int)typeof(Ships.CrewOperatedComponent).GetField("_crewRequired", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            crewOperatedComponentData.CrewRequired = _crewRequired;
                            crewOperatedComponentData.ReinforceCrewAt = ((Ships.CrewOperatedComponent)currComponent).ReinforceLevel;
                            componentData.CrewOperatedComponentData = crewOperatedComponentData;
                        }

                        if (currComponent is Ships.DCLockerComponent)
                        {
                            Logger.LogInfo($"Grabbing DCLockerComponent Info");
                            var dcLockerComponentData = new DCLockerComponentData();
                            var _movementSpeed = (float)typeof(Ships.DCLockerComponent).GetField("_movementSpeed", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _repairPerTick = (float)typeof(Ships.DCLockerComponent).GetField("_repairPerTick", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _canRestore = (bool)typeof(Ships.DCLockerComponent).GetField("_canRestore", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _restoreCount = (int)typeof(Ships.DCLockerComponent).GetField("_restoreCount", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            dcLockerComponentData.TeamsProduced = ((Ships.DCLockerComponent)currComponent).TeamsProduced;
                            dcLockerComponentData.TeamRepairRate = 4 * _repairPerTick;
                            dcLockerComponentData.MovementSpeed = 2.5f * 4f * _movementSpeed;
                            dcLockerComponentData.CanRestore = _canRestore;
                            dcLockerComponentData.RestoreCount = ((Ships.DCLockerComponent)currComponent).RestoresAvailable;
                            componentData.DCLockerComponentData = dcLockerComponentData;
                        }

                        if (currComponent is Ships.DiscreteWeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing DiscreteWeaponComponent Info");
                            var discreteWeaponComponentData = new DiscreteWeaponComponentData();
                            var _timeBetweenMuzzles = (float)typeof(Ships.DiscreteWeaponComponent).GetField("_timeBetweenMuzzles", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _magazineSize = (int)typeof(Ships.DiscreteWeaponComponent).GetField("_magazineSize", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _reloadTime = (float)typeof(Ships.DiscreteWeaponComponent).GetField("_reloadTime", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            discreteWeaponComponentData.TimeBetweenMuzzles = _timeBetweenMuzzles;
                            discreteWeaponComponentData.MagazineSize = _magazineSize;
                            discreteWeaponComponentData.ReloadTime = _reloadTime;
                            componentData.DiscreteWeaponComponentData = discreteWeaponComponentData;
                        }

                        if (currComponent is Ships.FixedContinuousWeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing FixedContinuousWeaponComponent Info");
                            var fixedContinuousWeaponComponentData = new FixedContinuousWeaponComponentData();
                            var _steerableBeam = (bool)typeof(Ships.FixedContinuousWeaponComponent).GetField("_steerableBeam", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _steerAngle = (float)typeof(Ships.FixedContinuousWeaponComponent).GetField("_steerAngle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _steerRate = (float)typeof(Ships.FixedContinuousWeaponComponent).GetField("_steerRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            fixedContinuousWeaponComponentData.SteerableBeam = _steerableBeam;
                            fixedContinuousWeaponComponentData.SteerAngle = _steerAngle;
                            fixedContinuousWeaponComponentData.SteerRate = _steerRate;
                            componentData.FixedContinuousWeaponComponentData = fixedContinuousWeaponComponentData;
                        }

                        if (currComponent is Ships.IntelligenceComponent)
                        {
                            Logger.LogInfo($"Grabbing IntelligenceComponent Info");
                            var intelligenceComponentData = new IntelligenceComponentData();
                            var _intelEffort = (float)typeof(Ships.IntelligenceComponent).GetField("_intelEffort", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _intelAccuracy = (float)typeof(Ships.IntelligenceComponent).GetField("_intelAccuracy", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            intelligenceComponentData.IntelEffort = _intelEffort;
                            intelligenceComponentData.IntelAccuracy = _intelAccuracy;
                            componentData.IntelligenceComponentData = intelligenceComponentData;
                        }

                        if (currComponent is Ships.OmnidirectionalEWarComponent)
                        {
                            Logger.LogInfo($"Grabbing OmnidirectionalEWarComponent Info");
                            var omnidirectionalEWarComponentData = new OmnidirectionalEWarComponentData();
                            var _sigType = (Game.Sensors.SignatureType)typeof(Ships.OmnidirectionalEWarComponent).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _maxRange = (float)typeof(Ships.OmnidirectionalEWarComponent).GetField("_maxRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _effectAreaRatio = (float)typeof(Ships.OmnidirectionalEWarComponent).GetField("_effectAreaRatio", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _radiatedPower = (float)typeof(Ships.OmnidirectionalEWarComponent).GetField("_radiatedPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _gain = (float)typeof(Ships.OmnidirectionalEWarComponent).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            omnidirectionalEWarComponentData.SigType = _sigType.ToString();
                            omnidirectionalEWarComponentData.MaxRange = _maxRange * 10;
                            omnidirectionalEWarComponentData.EffectAreaRatio = _effectAreaRatio;
                            omnidirectionalEWarComponentData.RadiatedPower = _radiatedPower;
                            omnidirectionalEWarComponentData.Gain = _gain;
                            componentData.OmnidirectionalEWarComponentData = omnidirectionalEWarComponentData;
                        }

                        if (currComponent is Ships.PassiveSensorComponent)
                        {
                            Logger.LogInfo($"Grabbing PassiveSensorComponent Info");
                            var passiveSensorComponentData = new PassiveSensorComponentData();
                            var _accuracy = (float)typeof(Ships.PassiveSensorComponent).GetField("_accuracy", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            passiveSensorComponentData.Accuracy = _accuracy;
                            componentData.PassiveSensorComponentData = passiveSensorComponentData;
                        }

                        if (currComponent is Ships.BaseActiveSensorComponent)
                        {
                            Logger.LogInfo($"Grabbing BaseActiveSensorComponent Info");
                            var sensorComponentData = new SensorComponentData();
                            var _sigType = (Game.Sensors.SignatureType)typeof(Ships.BaseActiveSensorComponent).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _maxRange = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_maxRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _radiatedPower = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_radiatedPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _gain = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _sensitivity = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_sensitivity", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _apertureSize = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_apertureSize", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _noiseFiltering = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_noiseFiltering", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _canLock = (bool)typeof(Ships.BaseActiveSensorComponent).GetField("_canLock", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _lockErrorMultiplier = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_lockErrorMultiplier", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _maintainLockSNR = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_maintainLockSNR", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _burnthroughPowerMultiplier = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_burnthroughPowerMultiplier", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _burnthroughDamageProbability = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_burnthroughDamageProbability", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _burnthroughDamage = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_burnthroughDamage", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            sensorComponentData.SigType = _sigType.ToString();
                            sensorComponentData.MaxRange = _maxRange * 10;
                            sensorComponentData.RadiatedPower = _radiatedPower;
                            sensorComponentData.Gain = _gain;
                            sensorComponentData.Sensitivity = _sensitivity;
                            sensorComponentData.ApertureSize = _apertureSize;
                            sensorComponentData.NoiseFiltering = _noiseFiltering;
                            sensorComponentData.CanLock = _canLock;
                            sensorComponentData.LockErrorMultiplier = _lockErrorMultiplier;
                            sensorComponentData.MaintainLockSNR = _maintainLockSNR;
                            sensorComponentData.BurnthroughPowerMultiplier = _burnthroughPowerMultiplier;
                            sensorComponentData.BurnthroughDamageProbability = _burnthroughDamageProbability;
                            sensorComponentData.BurnthroughDamage = _burnthroughDamage;
                            var deviationData = new DeviationData[101];
                            var _deviationCurve = (UnityEngine.AnimationCurve)typeof(Ships.BaseActiveSensorComponent).GetField("_deviationCurve", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            for (int i = 0; i <= 100; i++)
                            {
                                var currentDeviationData = new DeviationData();
                                currentDeviationData.SignalLoss = (((float)i) / 100);
                                currentDeviationData.DeviationMultiplier = _deviationCurve.Evaluate(((float)i) / 100);
                                deviationData[i] = currentDeviationData;
                            }
                            sensorComponentData.DeviationData = deviationData;
                            if (currComponent is Ships.OrderableActiveSensorComponent)
                            {
                                Logger.LogInfo($"Grabbing OrderableActiveSensorComponent Info");
                                var radiationData = new RadiationData[181];
                                var _radiationPattern = (UnityEngine.AnimationCurve)typeof(Ships.OrderableActiveSensorComponent).GetField("_radiationPattern", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                                for (int i = 0; i <= 180; i++)
                                {
                                    var currentRadiationData = new RadiationData();
                                    currentRadiationData.Direction = i;
                                    currentRadiationData.Value = _radiationPattern.Evaluate(((float)90 - i) / 90);
                                    radiationData[i] = currentRadiationData;
                                }
                                sensorComponentData.RadiationData = radiationData;
                            }
                            componentData.SensorComponentData = sensorComponentData;
                        }

                        Ships.FireControlSensor fireControl = null;
                        if (currComponent is Ships.WeaponComponent)
                        {
                            fireControl = (Ships.FireControlSensor)typeof(Ships.WeaponComponent).GetField("_integratedFireControl", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        }
                        if (currComponent is Ships.SensorTurretComponent)
                        {
                            fireControl = (Ships.ActiveFireControlSensor)typeof(Ships.SensorTurretComponent).GetField("_sensorPanel", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                        }
                        if (fireControl != null)
                        {
                            var fireControlData = new FireControlData();
                            var _sigType = (Game.Sensors.SignatureType)typeof(Ships.FireControlSensor).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                            var _maxRange = (float)typeof(Ships.FireControlSensor).GetField("_maxRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                            var _noiseFiltering = (float)typeof(Ships.FireControlSensor).GetField("_noiseFiltering", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                            fireControlData.SigType = _sigType.ToString();
                            fireControlData.MaxRange = _maxRange * 10;
                            fireControlData.NoiseFiltering = _noiseFiltering;
                            var deviationData = new DeviationData[101];
                            var _deviationCurve = (UnityEngine.AnimationCurve)typeof(Ships.FireControlSensor).GetField("_deviationCurve", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                            for (int i = 0; i <= 100; i++)
                            {
                                var currentDeviationData = new DeviationData();
                                currentDeviationData.SignalLoss = (((float)i) / 100);
                                currentDeviationData.DeviationMultiplier = _deviationCurve.Evaluate(((float)i) / 100);
                                deviationData[i] = currentDeviationData;
                            }
                            fireControlData.DeviationData = deviationData;
                            if (fireControl is Ships.ActiveFireControlSensor)
                            {
                                Logger.LogInfo($"Grabbing ActiveFireControlSensor Info");
                                var _radiatedPower = (float)typeof(Ships.ActiveFireControlSensor).GetField("_radiatedPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                var _gain = (float)typeof(Ships.ActiveFireControlSensor).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                var _apertureSize = (float)typeof(Ships.ActiveFireControlSensor).GetField("_apertureSize", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                var _lockErrorMultiplier = (float)typeof(Ships.ActiveFireControlSensor).GetField("_lockErrorMultiplier", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                var _maintainLockSNR = (float)typeof(Ships.ActiveFireControlSensor).GetField("_maintainLockSNR", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                fireControlData.RadiatedPower = _radiatedPower;
                                fireControlData.Gain = _gain;
                                fireControlData.ApertureSize = _apertureSize;
                                fireControlData.LockErrorMultiplier = _lockErrorMultiplier;
                                fireControlData.MaintainLockSNR = _maintainLockSNR;
                                var radiationData = new RadiationData[181];
                                var _radiationPattern = (UnityEngine.AnimationCurve)typeof(Ships.ActiveFireControlSensor).GetField("_radiationPattern", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(fireControl);
                                for (int i = 0; i <= 180; i++)
                                {
                                    var currentRadiationData = new RadiationData();
                                    currentRadiationData.Direction = i;
                                    currentRadiationData.Value = _radiationPattern.Evaluate(((float)90 - i) / 90);
                                    radiationData[i] = currentRadiationData;
                                }
                                fireControlData.RadiationData = radiationData;
                            }
                            componentData.FireControlData = fireControlData;
                        }

                        if (currComponent is Ships.SensorTurretComponent)
                        {
                            Logger.LogInfo($"Grabbing SensorTurretComponent Info");
                            var sensorTurretComponentData = new SensorTurretComponentData();
                            var _traverseRate = (float)typeof(Ships.SensorTurretComponent).GetField("_traverseRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _elevationRate = (float)typeof(Ships.SensorTurretComponent).GetField("_elevationRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _turretControl = (Ships.TurretController)typeof(Ships.SensorTurretComponent).GetField("_turretControl", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _minElevation = (float)typeof(Ships.TurretController).GetField("_minElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            var _maxElevation = (float)typeof(Ships.TurretController).GetField("_maxElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            sensorTurretComponentData.TraverseRate = _traverseRate;
                            sensorTurretComponentData.ElevationRate = _elevationRate;
                            sensorTurretComponentData.MinElevation = _minElevation;
                            sensorTurretComponentData.MaxElevation = _maxElevation;
                            componentData.SensorTurretComponentData = sensorTurretComponentData;
                        }

                        if (currComponent is Ships.TubeLauncherComponent)
                        {
                            Logger.LogInfo($"Grabbing TubeLauncherComponent Info");
                            var tubeLauncherComponentData = new TubeLauncherComponentData();
                            var _missileSize = (int)typeof(Ships.TubeLauncherComponent).GetField("_missileSize", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _loadTime = (float)typeof(Ships.TubeLauncherComponent).GetField("_loadTime", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _launchesPerLoad = (int)typeof(Ships.TubeLauncherComponent).GetField("_launchesPerLoad", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _timeBetweenLaunches = (float)typeof(Ships.TubeLauncherComponent).GetField("_timeBetweenLaunches", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            tubeLauncherComponentData.MissileSize = _missileSize;
                            tubeLauncherComponentData.LoadTime = _loadTime;
                            tubeLauncherComponentData.LaunchesPerLoad = _launchesPerLoad;
                            tubeLauncherComponentData.TimeBetweenLaunches = _timeBetweenLaunches;
                            componentData.TubeLauncherComponentData = tubeLauncherComponentData;
                        }

                        if (currComponent is Ships.TurretedContinuousWeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing TurretedContinuousWeaponComponent Info");
                            var turretedContinuousWeaponComponentData = new TurretedContinuousWeaponComponentData();
                            var _traverseRate = (float)typeof(Ships.TurretedContinuousWeaponComponent).GetField("_traverseRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _elevationRate = (float)typeof(Ships.TurretedContinuousWeaponComponent).GetField("_elevationRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _limitRotationSpeedWhenFiring = (bool)typeof(Ships.TurretedContinuousWeaponComponent).GetField("_limitRotationSpeedWhenFiring", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _firingRotationSpeedModifier = (float)typeof(Ships.TurretedContinuousWeaponComponent).GetField("_firingRotationSpeedModifier", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _turretControl = (Ships.TurretController)typeof(Ships.TurretedContinuousWeaponComponent).GetField("_turretControl", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _minElevation = (float)typeof(Ships.TurretController).GetField("_minElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            var _maxElevation = (float)typeof(Ships.TurretController).GetField("_maxElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            turretedContinuousWeaponComponentData.TraverseRate = _traverseRate;
                            turretedContinuousWeaponComponentData.ElevationRate = _elevationRate;
                            turretedContinuousWeaponComponentData.LimitRotationSpeedWhenFiring = _limitRotationSpeedWhenFiring;
                            turretedContinuousWeaponComponentData.FiringRotationSpeedModifier = _firingRotationSpeedModifier;
                            turretedContinuousWeaponComponentData.MinElevation = _minElevation;
                            turretedContinuousWeaponComponentData.MaxElevation = _maxElevation;
                            componentData.TurretedContinuousWeaponComponentData = turretedContinuousWeaponComponentData;
                        }

                        if (currComponent is Ships.TurretedDiscreteWeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing TurretedDiscreteWeaponComponent Info");
                            var turretedDiscreteWeaponComponentData = new TurretedDiscreteWeaponComponentData();
                            var _traverseRate = (float)typeof(Ships.TurretedDiscreteWeaponComponent).GetField("_traverseRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _elevationRate = (float)typeof(Ships.TurretedDiscreteWeaponComponent).GetField("_elevationRate", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _turretControl = (Ships.TurretController)typeof(Ships.TurretedDiscreteWeaponComponent).GetField("_turretControl", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _minElevation = (float)typeof(Ships.TurretController).GetField("_minElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            var _maxElevation = (float)typeof(Ships.TurretController).GetField("_maxElevation", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_turretControl);
                            turretedDiscreteWeaponComponentData.TraverseRate = _traverseRate;
                            turretedDiscreteWeaponComponentData.ElevationRate = _elevationRate;
                            turretedDiscreteWeaponComponentData.MinElevation = _minElevation;
                            turretedDiscreteWeaponComponentData.MaxElevation = _maxElevation;
                            componentData.TurretedDiscreteWeaponComponentData = turretedDiscreteWeaponComponentData;
                        }

                        if (currComponent is Ships.TurretedEWarComponent)
                        {
                            Logger.LogInfo($"Grabbing TurretedEWarComponent Info");
                            var turretedEWarComponentData = new TurretedEWarComponentData();
                            var _coneFov = (float)typeof(Ships.TurretedEWarComponent).GetField("_coneFov", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _maxRange = (float)typeof(Ships.TurretedEWarComponent).GetField("_maxRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _effectAreaRatio = (float)typeof(Ships.TurretedEWarComponent).GetField("_effectAreaRatio", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _radiatedPower = (float)typeof(Ships.TurretedEWarComponent).GetField("_radiatedPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _gain = (float)typeof(Ships.TurretedEWarComponent).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            turretedEWarComponentData.ConeFov = _coneFov;
                            turretedEWarComponentData.MaxRange = _maxRange * 10;
                            turretedEWarComponentData.EffectAreaRatio = _effectAreaRatio;
                            turretedEWarComponentData.RadiatedPower = _radiatedPower;
                            turretedEWarComponentData.Gain = _gain;
                            componentData.TurretedEWarComponentData = turretedEWarComponentData;
                        }

                        if (currComponent is Ships.WeaponComponent)
                        {
                            Logger.LogInfo($"Grabbing WeaponComponent Info");
                            var weaponComponentData = new WeaponComponentData();
                            var _optimalTargetWeight = (Utility.WeightClass)typeof(Ships.WeaponComponent).GetField("_optimalTargetWeight", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _allowGrouping = (bool)typeof(Ships.WeaponComponent).GetField("_allowGrouping", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _groupingName = (string)typeof(Ships.WeaponComponent).GetField("_groupingName", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _role = (Utility.WeaponRole)typeof(Ships.WeaponComponent).GetField("_role", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _ewType = (Game.EWar.EWarWeaponType)typeof(Ships.WeaponComponent).GetField("_ewType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _checkFriendlyFire = (bool)typeof(Ships.WeaponComponent).GetField("_checkFriendlyFire", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _checkObstaclesInWay = (bool)typeof(Ships.WeaponComponent).GetField("_checkObstaclesInWay", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _requireExternalAmmoFeed = (bool)typeof(Ships.WeaponComponent).GetField("_requireExternalAmmoFeed", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _allowAmmoSelection = (bool)typeof(Ships.WeaponComponent).GetField("_allowAmmoSelection", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _leadTargets = (bool)typeof(Ships.WeaponComponent).GetField("_leadTargets", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _onTargetAngle = (float)typeof(Ships.WeaponComponent).GetField("_onTargetAngle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            var _muzzles = (Ships.Muzzle[])typeof(Ships.WeaponComponent).GetField("_muzzles", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currComponent);
                            weaponComponentData.OptimalTargetWeight = _optimalTargetWeight.ToString();
                            weaponComponentData.AllowGrouping = _allowGrouping;
                            weaponComponentData.GroupingName = _groupingName;
                            weaponComponentData.Role = _role.ToDetailsString() + "^^(" + _role.ToString() + ")^^";//displaying both because bitwise behavior can cause two ways for damage to become listed as dual purpose. probably doesn't matter in practice but might be useful data to know
                            weaponComponentData.EwType = _ewType.ToString();
                            weaponComponentData.CheckFriendlyFire = _checkFriendlyFire;
                            weaponComponentData.CheckObstaclesInWay = _checkObstaclesInWay;
                            weaponComponentData.RequireExternalAmmoFeed = _requireExternalAmmoFeed;
                            weaponComponentData.AllowAmmoSelection = _allowAmmoSelection;
                            weaponComponentData.LeadTargets = _leadTargets;
                            weaponComponentData.OnTargetAngle = _onTargetAngle;
                            if (_muzzles.Length > 0)
                            {
                                var muzzle = _muzzles[0];
                                var muzzleData = new MuzzleData();
                                if (muzzle is Ships.RaycastMuzzle)
                                {
                                    Logger.LogInfo($"Grabbing RaycastMuzzle Info");
                                    var damageCharacteristicData = new DamageCharacteristicData();
                                    var _armorPenetration = (float)typeof(Ships.RaycastMuzzle).GetField("_armorPenetration", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    var _heatPower = (float)typeof(Ships.RaycastMuzzle).GetField("_heatPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    var _componentDamage = (float)typeof(Ships.RaycastMuzzle).GetField("_componentDamage", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    damageCharacteristicData.ArmorPenetration = _armorPenetration;
                                    damageCharacteristicData.OverpenetrationDamageMultiplier = ((Munitions.IDamageCharacteristic)muzzle).OverpenetrationDamageMultiplier;
                                    damageCharacteristicData.MaxPenetrationDepth = ((float)typeof(Ships.RaycastMuzzle).GetField("_maxPenetrationDistance", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle))*10;
                                    damageCharacteristicData.HeatDamage = _heatPower;
                                    damageCharacteristicData.DamageBrushSize = ((Munitions.IDamageCharacteristic)muzzle).DamageBrushSize;
                                    damageCharacteristicData.ComponentDamage = _componentDamage;
                                    if (muzzle is IDirectDamageMuzzle)
                                    {
                                        muzzleData.damagePeriod = ((IDirectDamageMuzzle)muzzle).DamagePeriod;
                                        muzzleData.armorPerSecond = damageCharacteristicData.ArmorPenetration / muzzleData.damagePeriod;
                                        muzzleData.damagePerSecond = damageCharacteristicData.ComponentDamage / muzzleData.damagePeriod;
                                    }
                                    
                                    damageCharacteristicData.RandomEffectMultiplier = ((Munitions.IDamageCharacteristic)muzzle).RandomEffectMultiplier;
                                    damageCharacteristicData.CrewVulnerabilityMultiplier = ((Munitions.IDamageCharacteristic)muzzle).CrewVulnerabilityMultiplier;
                                    damageCharacteristicData.IgnoreEffectiveThickness = ((Munitions.IDamageCharacteristic)muzzle).IgnoreEffectiveThickness;
                                    damageCharacteristicData.NeverRicochet = ((Munitions.IDamageCharacteristic)muzzle).NeverRicochet;
                                    componentData.RaycastDamageCharacteristicData = damageCharacteristicData;
                                    var damageFalloffData = new DamageFalloffData[101];
                                    var _raycastRange = (float)typeof(Ships.RaycastMuzzle).GetField("_raycastRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    var _powerFalloff = (UnityEngine.AnimationCurve)typeof(Ships.RaycastMuzzle).GetField("_powerFalloff", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    for (int i = 0; i <= 100; i++)
                                    {
                                        var currentDamageFalloffData = new DamageFalloffData();
                                        currentDamageFalloffData.Range = (((float)i) / 100) * _raycastRange * 10;
                                        currentDamageFalloffData.DamageModifier = _powerFalloff.Evaluate(((float)i) / 100);
                                        damageFalloffData[i] = currentDamageFalloffData;
                                    }
                                    componentData.RaycastDamageFalloffData = damageFalloffData;
                                }
                                if (muzzle is Ships.Muzzle)
                                {
                                    Logger.LogInfo($"Grabbing Muzzle Info");
                                    muzzleData.SimMethod = ((Ships.Muzzle)muzzle).SimMethod.ToString();
                                }
                                if (muzzle is Ships.IRangedMuzzle)
                                {
                                    Logger.LogInfo($"Grabbing IRangedMuzzle Info");
                                    muzzleData.MaxRange = ((Ships.IRangedMuzzle)muzzle).MaxRange * 10;
                                }
                                if (muzzle is Ships.AccuracyMuzzle)
                                {
                                    Logger.LogInfo($"Grabbing AccuracyMuzzle Info");
                                    var _ignoreAccuracyModifiers = (bool)typeof(Ships.AccuracyMuzzle).GetField("_ignoreAccuracyModifiers", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    var _baseAccuracy = (float)typeof(Ships.AccuracyMuzzle).GetField("_baseAccuracy", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
                                    muzzleData.IgnoreAccuracyModifiers = _ignoreAccuracyModifiers;
                                    muzzleData.BaseAccuracy = _baseAccuracy;
                                    var accuracyData = new AccuracyData[21];
                                    for (int i = 0; i <= 20; i++)
                                    {
                                        var currentAccuracyData = new AccuracyData();
                                        float range = i * 100f;
                                        float spread = range * Mathf.Tan((float)(Math.PI / 180.0 * (double)_baseAccuracy * 2.0));
                                        currentAccuracyData.Range = range * 10f;
                                        currentAccuracyData.Spread = spread * 10f;
                                        accuracyData[i] = currentAccuracyData;
                                    }
                                    muzzleData.AccuracyData = accuracyData;
                                }

                                

                                componentData.MuzzleData = muzzleData;
                            }

                            componentData.WeaponComponentData = weaponComponentData;
                        }

                        if (currComponent is Game.EWar.IEWarTarget)
                        {
                            Logger.LogInfo($"Grabbing IEWarTarget Info");
                            var iEWarTargetData = new IEWarTargetData();
                            iEWarTargetData.SigType = ((Game.EWar.IEWarTarget)currComponent).SigType.ToString();
                            componentData.IEWarTargetData = iEWarTargetData;
                        }

                        if (currComponent is Ships.Controls.IIntelComponent)
                        {
                            Logger.LogInfo($"Grabbing IIntelComponent Info");
                            var iIntelComponentData = new IIntelComponentData();
                            iIntelComponentData.WorkOnRemoteTracks = ((Ships.Controls.IIntelComponent)currComponent).WorkOnRemoteTracks;
                            componentData.IIntelComponentData = iIntelComponentData;
                        }

                        if (currComponent is Ships.Controls.IMagazineProvider)
                        {
                            Logger.LogInfo($"Grabbing IMagazineProvider Info");
                            var iMagazineProviderData = new IMagazineProviderData();
                            iMagazineProviderData.VolumeBased = ((Ships.Controls.IMagazineProvider)currComponent).VolumeBased;
                            iMagazineProviderData.CanFeedExternally = ((Ships.Controls.IMagazineProvider)currComponent).CanFeedExternally;
                            iMagazineProviderData.CanProvide = ((Ships.Controls.IMagazineProvider)currComponent).CanProvide;
                            iMagazineProviderData.ProviderKey = ((Ships.Controls.IMagazineProvider)currComponent).ProviderKey;
                            componentData.IMagazineProviderData = iMagazineProviderData;
                        }

                        if (currComponent is Ships.Controls.ISensorComponent)
                        {
                            Logger.LogInfo($"Grabbing ISensorComponent Info");
                            var iSensorComponentData = new ISensorComponentData();
                            iSensorComponentData.IgnoreEMCON = ((Ships.Controls.ISensorComponent)currComponent).IgnoreEMCON;
                            iSensorComponentData.CanOrderLock = ((Ships.Controls.ISensorComponent)currComponent).CanOrderLock;
                            iSensorComponentData.CanOrderBurnthrough = ((Ships.Controls.ISensorComponent)currComponent).CanOrderBurnthrough;
                            componentData.ISensorComponentData = iSensorComponentData;
                        }

                        if (currComponent is Ships.Controls.IWeapon)
                        {
                            Logger.LogInfo($"Grabbing IWeapon Info");
                            var iWeaponData = new IWeaponData();
                            iWeaponData.WepType = ((Ships.Controls.IWeapon)currComponent).WepType.ToString();
                            iWeaponData.OptimalTargetWeight = ((Ships.Controls.IWeapon)currComponent).OptimalTargetWeight.ToString();
                            iWeaponData.PDTTargetMethod = ((Ships.Controls.IWeapon)currComponent).PDTTargetMethod.ToString();
                            iWeaponData.NeedsExternalAmmoFeed = ((Ships.Controls.IWeapon)currComponent).NeedsExternalAmmoFeed;
                            iWeaponData.SupportsAmmoSelection = ((Ships.Controls.IWeapon)currComponent).SupportsAmmoSelection;
                            try
                            {
                                iWeaponData.SupportsPositionTargeting = ((Ships.Controls.IWeapon)currComponent).SupportsPositionTargeting;
                                iWeaponData.PositionTargetingNoInput = ((Ships.Controls.IWeapon)currComponent).PositionTargetingNoInput;
                                iWeaponData.SupportsTrackTargeting = ((Ships.Controls.IWeapon)currComponent).SupportsTrackTargeting;
                                iWeaponData.SupportsVisualTargeting = ((Ships.Controls.IWeapon)currComponent).SupportsVisualTargeting;
                                iWeaponData.SupportsWaypoints = ((Ships.Controls.IWeapon)currComponent).SupportsWaypoints;
                            }
                            catch (Exception e)
                            {
                                iWeaponData.SupportsPositionTargeting = null;
                                iWeaponData.PositionTargetingNoInput = null;
                                iWeaponData.SupportsTrackTargeting = null;
                                iWeaponData.SupportsVisualTargeting = null;
                                iWeaponData.SupportsWaypoints = null;
                            }
                            componentData.IWeaponData = iWeaponData;
                        }
                        var json = JsonConvert.SerializeObject(componentData);
                        Logger.LogInfo($"Serialized JSON for {currComponent.ComponentName}: {json}");

                        UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/component", json);
                        webRequest.SetRequestHeader("Content-Type", "application/json");
                        webRequest.SendWebRequest();
                    }
                    this._phase++;
                }
                else if (this._phase == 7)
                {
                    //Start sending munition data
                    foreach (var currMunition in Bundles.BundleManager.Instance.AllMunitions)
                    {
                        Logger.LogInfo($"Found munition {currMunition.MunitionName}");
                        try
                        {
                            var screenshotPath = this._filepath + "\\" + this.FormatName(currMunition.MunitionName) + "_screenshot.png";
                            var screenshotTexture = currMunition.DetailScreenshot.texture;
                            if (!TextureHelper.IsReadable(screenshotTexture))
                            {
                                screenshotTexture = TextureHelper.ForceReadTexture(screenshotTexture);
                            }
                            byte[] screenshotData = TextureHelper.EncodeToPNG(screenshotTexture);
                            File.WriteAllBytes(screenshotPath, screenshotData);
                            Logger.LogInfo($"Saving {currMunition.MunitionName} screenshot to {screenshotPath}");
                        }
                        catch (NullReferenceException e)
                        {
                            Logger.LogInfo($"Screenshot not found, skipping {currMunition.MunitionName}");
                        }

                        try
                        {
                            if (currMunition is Munitions.Missile)
                            {
                                var iconPath = this._filepath + "\\" + this.FormatName(currMunition.MunitionName) + "_icon.png";
                                var iconTexture = ((Munitions.Missile)currMunition).Icon.texture;
                                if (!TextureHelper.IsReadable(iconTexture))
                                {
                                    iconTexture = TextureHelper.ForceReadTexture(iconTexture);
                                }
                                byte[] iconData = TextureHelper.EncodeToPNG(iconTexture);
                                File.WriteAllBytes(iconPath, iconData);
                                Logger.LogInfo($"Saving {currMunition.MunitionName} icon to {iconPath}");
                            }
                        }
                        catch (NullReferenceException e)
                        {
                            Logger.LogInfo($"Icon not found, skipping {currMunition.MunitionName}");
                        }
                        var munitionData = new MunitionData();
                        munitionData.MunitionName = currMunition.MunitionName;
                        munitionData.SaveKey = currMunition.SaveKey;
                        munitionData.Type = currMunition.Type.ToString();
                        munitionData.SimMethod = currMunition.SimMethod.ToString();
                        munitionData.Role = currMunition.Role.ToDetailsString() + "^^(" + currMunition.Role.ToString()+ ")^^";//displaying both because bitwise behavior can cause two ways for shells to become listed as dual purpose. probably doesn't matter in practice but might be useful data to know
                        munitionData.PointCost = currMunition.PointCost;
                        munitionData.PointDivision = currMunition.PointDivision;
                        munitionData.StorageVolume = currMunition.StorageVolume;
                        munitionData.SupportsPositionTargeting = currMunition.SupportsPositionTargeting;
                        munitionData.PositionTargetingNoInput = currMunition.PositionTargetingNoInput;
                        munitionData.SupportsTrackTargeting = currMunition.SupportsTrackTargeting;
                        munitionData.SupportsVisualTargeting = currMunition.SupportsVisualTargeting;
                        munitionData.SupportsWaypoints = currMunition.SupportsWaypoints;
                        munitionData.FlightSpeed = currMunition.FlightSpeed * 10;
                        munitionData.Lifetime = currMunition.Lifetime;
                        munitionData.MaxRange = currMunition.MaxRange * 10;
                        munitionData.Description = currMunition.GetDetailText();

                        if (currMunition.DamageCharacteristics != null)
                        {
                            Logger.LogInfo($"Grabbing DamageCharacteristics Info");
                            var damageCharacteristicData = new DamageCharacteristicData();
                            damageCharacteristicData.ArmorPenetration = currMunition.DamageCharacteristics.ArmorPenetration;
                            damageCharacteristicData.OverpenetrationDamageMultiplier = currMunition.DamageCharacteristics.OverpenetrationDamageMultiplier;
                            damageCharacteristicData.MaxPenetrationDepth = currMunition.DamageCharacteristics.MaxPenetrationDepth*10;
                            damageCharacteristicData.HeatDamage = currMunition.DamageCharacteristics.HeatDamage;
                            damageCharacteristicData.DamageBrushSize = currMunition.DamageCharacteristics.DamageBrushSize;
                            damageCharacteristicData.ComponentDamage = currMunition.DamageCharacteristics.ComponentDamage;
                            damageCharacteristicData.RandomEffectMultiplier = currMunition.DamageCharacteristics.RandomEffectMultiplier;
                            damageCharacteristicData.CrewVulnerabilityMultiplier = currMunition.DamageCharacteristics.CrewVulnerabilityMultiplier;
                            damageCharacteristicData.IgnoreEffectiveThickness = currMunition.DamageCharacteristics.IgnoreEffectiveThickness;
                            damageCharacteristicData.NeverRicochet = currMunition.DamageCharacteristics.NeverRicochet;
                            munitionData.DamageCharacteristics = damageCharacteristicData;
                        }
                        
                        if (currMunition is Munitions.ShellMunition)
                        {
                            Logger.LogInfo($"Grabbing ShellMunition Info");
                            var shellMunitionData = new LWMunitionData();
                            var _maxPenetrationDistance = (float)typeof(Munitions.ShellMunition).GetField("_maxPenetrationDistance", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            shellMunitionData.MaxPenetration = _maxPenetrationDistance * 10;
                            /*
                            var penetrationFalloffData = new PenetrationFalloffData[1001];
                            var _penetrationFalloff = (UnityEngine.AnimationCurve)typeof(Munitions.ShellMunition).GetField("_penetrationFalloff", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            for(int i = 0;i <= 1000;i++) {
                              var currentPenetrationFalloffData = new PenetrationFalloffData();
                              currentPenetrationFalloffData.Range = (((float)i) / 1000) * currMunition.Lifetime * currMunition.MaxRange * 10;
                              currentPenetrationFalloffData.Penetration = _maxPenetrationDistance * 10 * _penetrationFalloff.Evaluate(((float)i) / 1000);
                              penetrationFalloffData[i] = currentPenetrationFalloffData;
                            }
                            shellMunitionData.PenetrationFalloffData = penetrationFalloffData;
                            */
                            munitionData.LWMunitionData = shellMunitionData;
                        }
                        
                        if (currMunition is Munitions.LightweightMunitionBase)
                        {
                            Logger.LogInfo($"Grabbing LightweightMunitionBase Info");
                            var lwMunitionData = new LWMunitionData();
                            var _maxPenetrationDistance = (float)typeof(Munitions.LightweightMunitionBase).GetField("_maxPenetrationDistance", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            lwMunitionData.MaxPenetration = _maxPenetrationDistance * 10;
                            
                            //this field seems to be just a description, rather than flavor text. The description is already filled earlier so this is redundant   
                            //munitionData.FlavorText = (string)typeof(Munitions.LightweightMunitionBase).GetField("_flavorText", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                           
                            /*
                            var penetrationFalloffData = new PenetrationFalloffData[1001];
                            var _penetrationFalloff = (UnityEngine.AnimationCurve)typeof(Munitions.LightweightMunitionBase).GetField("_penetrationFalloff", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            for(int i = 0;i <= 1000;i++) {
                              var currentPenetrationFalloffData = new PenetrationFalloffData();
                              currentPenetrationFalloffData.Range = (((float)i) / 1000) * currMunition.Lifetime * currMunition.MaxRange * 10;
                              currentPenetrationFalloffData.Penetration = _maxPenetrationDistance * 10 * _penetrationFalloff.Evaluate(((float)i) / 1000);
                              penetrationFalloffData[i] = currentPenetrationFalloffData;
                            }
                            shellMunitionData.PenetrationFalloffData = penetrationFalloffData;
                            */
                            munitionData.LWMunitionData = lwMunitionData;
                        }
                        if (currMunition is Munitions.Missile)
                        {
                            var missileData = new MissileData();
                            var _size = (int)typeof(Munitions.Missile).GetField("_size", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            missileData.Size = _size;
                            munitionData.FlavorText = (string)typeof(Munitions.Missile).GetField("_optionalFlavorText", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            munitionData.MissileData = missileData;
                        }

                        if(currMunition is LightweightExplosiveShell)
                        {
                            munitionData.PenetrationEnvelope = (float)typeof(LightweightExplosiveShell).GetField("_penetrationEnvelope", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            
                            munitionData.ExplosionRadius = 10 * (float)typeof(LightweightExplosiveShell).GetField("_explosionRadius", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMunition);
                            
                        }

                        var json = JsonConvert.SerializeObject(munitionData);
                        Logger.LogInfo($"Serialized JSON for {currMunition.MunitionName}: {json}");

                        UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/munition", json);
                        webRequest.SetRequestHeader("Content-Type", "application/json");
                        webRequest.SendWebRequest();
                    }
                    this._phase++;
                }
                
                else if (this._phase == 8)
                {
                    /*
                    //Open Create Ship modal
                    var foundMissileEditorSubmodeController = FindObjectsOfType<FleetEditor.MissileEditorSubmodeController>();
                    if(foundMissileEditorSubmodeController.Length > 0) {
                      Logger.LogInfo($"Found MissileEditorSubmodeController, creating ships");
                      var missileEditorSubmodeController = foundMissileEditorSubmodeController[0];
                      foreach(var currMissileBody in Bundles.BundleManager.Instance.AllMissileBodies) {
                        missileEditorSubmodeController.CreateNewMissile(currMissileBody);
                      }
                      this._phase++;
                    }
                    */
                    this._phase++;
                    
                }

               
                else if (this._phase == 9)
                {
                    /*
                   //Start sending missile body data
                   var foundMissileBodies = FindObjectsOfType<Munitions.ModularMissiles.ModularMissile>();
                   foreach(var currMissileBody in foundMissileBodies) {
                     Logger.LogInfo($"Found missile body {currMissileBody.MunitionName}");
                     try {
                       var screenshotPath = this._filepath + "\\" + this.FormatName(currMissileBody.MunitionName) + "_screenshot.png";
                       var screenshotTexture = currMissileBody.DetailScreenshot.texture;
                       if(!TextureHelper.IsReadable(screenshotTexture)) {
                         screenshotTexture = TextureHelper.ForceReadTexture(screenshotTexture);
                       }
                       byte[] screenshotData = TextureHelper.EncodeToPNG(screenshotTexture);
                       File.WriteAllBytes(screenshotPath, screenshotData);
                       Logger.LogInfo($"Saving {currMissileBody.MunitionName} screenshot to {screenshotPath}");
                     }
                     catch(NullReferenceException e) {
                       Logger.LogInfo($"Screenshot not found, skipping {currMissileBody.MunitionName}");
                     }

                     try {
                       if(currMissileBody is Munitions.Missile) {
                         var iconPath = this._filepath + "\\" + this.FormatName(currMissileBody.MunitionName) + "_icon.png";
                         var iconTexture = ((Munitions.Missile)currMissileBody).Icon.texture;
                         if(!TextureHelper.IsReadable(iconTexture)) {
                           iconTexture = TextureHelper.ForceReadTexture(iconTexture);
                         }
                         byte[] iconData = TextureHelper.EncodeToPNG(iconTexture);
                         File.WriteAllBytes(iconPath, iconData);
                         Logger.LogInfo($"Saving {currMissileBody.MunitionName} icon to {iconPath}");
                       }
                     }
                     catch(NullReferenceException e) {
                       Logger.LogInfo($"Icon not found, skipping {currMissileBody.MunitionName}");
                     }
                     var munitionData = new MunitionData();
                     munitionData.MunitionName = currMissileBody.MunitionName;
                     munitionData.PointCost = currMissileBody.PointCost;
                     munitionData.PointDivision = currMissileBody.PointDivision;
                     munitionData.Description = currMissileBody.GetDetailText();

                     if(currMissileBody.DamageCharacteristics != null) {
                       Logger.LogInfo($"Grabbing DamageCharacteristics Info");
                       var damageCharacteristicData = new DamageCharacteristicData();
                       damageCharacteristicData.ArmorPenetration = currMissileBody.DamageCharacteristics.ArmorPenetration;
                       damageCharacteristicData.OverpenetrationDamageMultiplier = currMissileBody.DamageCharacteristics.OverpenetrationDamageMultiplier;
                       damageCharacteristicData.MaxPenetrationDistance = currMissileBody.DamageCharacteristics.MaxPenetrationDepth;
                       damageCharacteristicData.HeatDamage = currMissileBody.DamageCharacteristics.HeatDamage;
                       damageCharacteristicData.DamageBrushSize = currMissileBody.DamageCharacteristics.DamageBrushSize;
                       damageCharacteristicData.ComponentDamage = currMissileBody.DamageCharacteristics.ComponentDamage;
                       damageCharacteristicData.RandomEffectMultiplier = currMissileBody.DamageCharacteristics.RandomEffectMultiplier;
                       damageCharacteristicData.CrewVulnerabilityMultiplier = currMissileBody.DamageCharacteristics.CrewVulnerabilityMultiplier;
                       damageCharacteristicData.IgnoreEffectiveThickness = currMissileBody.DamageCharacteristics.IgnoreEffectiveThickness;
                       damageCharacteristicData.NeverRicochet = currMissileBody.DamageCharacteristics.NeverRicochet;
                       munitionData.DamageCharacteristics = damageCharacteristicData;
                     }

                     if(currMissileBody is Munitions.Missile) {
                       var missileData = new MissileData();
                       var _size = (int)typeof(Munitions.Missile).GetField("_size", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMissileBody);
                       missileData.Size = _size;
                       var _radarSignature = (Game.Sensors.Signature)typeof(Munitions.ModularMissiles.ModularMissile).GetField("_radarSignature", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMissileBody);
                       var _samplePositions = (Vector3[])typeof(Game.Sensors.CrossSection).GetField("_samplePositions", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(null);
                       var _neighbors = (int[,])typeof(Game.Sensors.CrossSection).GetField("_neighbors", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(null);
                       missileData.SignatureData = new SignatureData[162];
                       for(int i = 0;i <= 161;i++) {
                         var currSignatureData = new SignatureData();
                         currSignatureData.x = _samplePositions[i].x;
                         currSignatureData.y = _samplePositions[i].y;
                         currSignatureData.z = _samplePositions[i].z;
                         currSignatureData.neighbors = new int[6];
                         for(int j = 0;j < 6;j++) {
                           currSignatureData.neighbors[j] = _neighbors[i,j];
                         }
                         currSignatureData.value = _radarSignature.GetCrossSection(-_samplePositions[i]) * 10;
                         missileData.SignatureData[i] = currSignatureData;
                       }
                       munitionData.MissileData = missileData;
                     }

                     var json = JsonConvert.SerializeObject(munitionData);
                     Logger.LogInfo($"Serialized JSON for {currMissileBody.MunitionName}: {json}");

                     UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/munition", json);
                     webRequest.SetRequestHeader("Content-Type", "application/json");
                     webRequest.SendWebRequest();
                   }
                   */
                   this._phase++;
                }
                
                else if (this._phase == 10)
                {
                    //Wait 30 seconds
                    this._updateInterval = 30.0f;
                    this._phase++;
                }
                else if (this._phase == 11)
                {
                    //Send end signal
                    Logger.LogInfo($"Sending end signal");
                    UnityWebRequest webRequest = UnityWebRequest.Put("http://localhost:9997/end", "{}");
                    webRequest.SetRequestHeader("Content-Type", "application/json");
                    webRequest.SendWebRequest();
                    this._updateInterval = 2.0f;
                    this._phase++;
                }
                else if (this._phase == 12)
                {
                    Application.Quit();
                }
            }
        }
        private void SaveFilePath(string path)
        {
            this._filepath = path;
        }
        private IEnumerator GetFilePath(string url, Action<string> pathCallback)
        {
            UnityWebRequest webRequest = UnityWebRequest.Get(url);
            //Request and wait for the desired string
            yield return webRequest.SendWebRequest();

            if (webRequest.result != UnityWebRequest.Result.Success)
            {
                Logger.LogInfo(webRequest.error);
            }
            else
            {
                //Save file path
                pathCallback(webRequest.downloadHandler.text);
            }
        }
        private string FormatName(string name)
        {
            return name.Replace(" ", "_").Replace("'", "").Replace("\"", "").Replace(".", "").ToLower();
        }
    }
}
