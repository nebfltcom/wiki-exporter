exports.generateRadiationPatternChart = (data) => {
  for(let i = data.length - 1;i > 0;i--) {
    data.push({
      Direction: -data[i].Direction,
      Value: data[i].Value
    });
  }
  
  let chartData = {
    labels: data.map(e => `${e.Direction}`),
    datasets: [{
      label: 'Radiation Pattern',
      data: data.map(e => e.Value * 100),
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor: 'rgba(54, 162, 235, 0.5)',
      fill: {
        target: 'start',
        below: 'rgba(54, 162, 235, 0.25)'
      }
    }]
  };
  let chartOption = {
    scales: {
      r: {
        beginAtZero: true,
        pointLabels: {
          display: false
        }
      }
    }
  };
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<canvas id="chartCanvas"></canvas>
<script>
  const config = {
    type: 'radar',
    data: ${JSON.stringify(chartData, null, 2)},
    options: ${JSON.stringify(chartOption, null, 2)}
  };
  const chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
</script>
[[/html]]
`;
}

exports.generateDamageFalloffChart = (baseDamage, data) => {
  let chartData = {
    labels: data.map(e => `${e.Range} m`),
    datasets: [{
      label: 'Damage Falloff',
      data: data.map(e => baseDamage * e.DamageModifier),
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor: 'rgba(54, 162, 235, 0.5)',
      fill: {
        target: 'start',
        below: 'rgba(54, 162, 235, 0.25)'
      }
    }]
  };
  let chartOption = {
    scales: {
      x: {
        title: {
          display: true,
          text: 'Range'
        }
      },
      y: {
        title: {
          display: true,
          text: 'Component Damage (hp/s)'
        }
      }
    }
  };
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<canvas id="chartCanvas"></canvas>
<script>
  const config = {
    type: 'line',
    data: ${JSON.stringify(chartData, null, 2)},
    options: ${JSON.stringify(chartOption, null, 2)}
  };
  const chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
</script>
[[/html]]
`;
}

exports.generatePenetrationFalloffChart = (data, maxRange) => {
  let chartData = {
    labels: data.filter(e => maxRange ? e.Range <= maxRange : true).map(e => `${e.Range} m`),
    datasets: [{
      label: 'Penetration Falloff',
      data: data.filter(e => maxRange ? e.Range <= maxRange : true).map(e => e.Penetration),
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor: 'rgba(54, 162, 235, 0.5)',
      fill: {
        target: 'start',
        below: 'rgba(54, 162, 235, 0.25)'
      }
    }]
  };
  let chartOption = {
    scales: {
      x: {
        title: {
          display: true,
          text: 'Range'
        }
      },
      y: {
        title: {
          display: true,
          text: 'Penetration Falloff (m)'
        }
      }
    }
  };
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<canvas id="chartCanvas"></canvas>
<script>
  const config = {
    type: 'line',
    data: ${JSON.stringify(chartData, null, 2)},
    options: ${JSON.stringify(chartOption, null, 2)}
  };
  const chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
</script>
[[/html]]
`;
}

exports.generateAccuracyChart = (data, maxRange) => {
  let chartData = {
    labels: data.filter(e => maxRange ? e.Range <= maxRange : true).map(e => `${(e.Range/1000).toFixed(2)} km`),
    datasets: [{
      label: 'Shot Spread',
      data: data.filter(e => maxRange ? e.Range <= maxRange : true).map(e => e.Spread),
      borderColor: 'rgb(75, 192, 192)',
      backgroundColor: 'rgba(75, 192, 192, 0.5)',
      fill: {
        target: 'start',
        below: 'rgba(75, 192, 192, 0.25)'
      }
    }]
  };
  let chartOption = {
    scales: {
      x: {
        title: {
          display: true,
          text: 'Range'
        }
      },
      y: {
        title: {
          display: true,
          text: 'Shot Spread (m)'
        }
      }
    }
  };
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<canvas id="chartCanvas"></canvas>
<script>
  const config = {
    type: 'line',
    data: ${JSON.stringify(chartData, null, 2)},
    options: ${JSON.stringify(chartOption, null, 2)}
  };
  const chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
</script>
[[/html]]
`;
}

exports.generateBattleshortLengthProbabilityChart = (baseHP, thresholdHP, overheatDamageProbability, overheatDamagePerSecond) => {
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div style="width: 100%; margin: 10px;">
  <label for="baseHPInput">Base Component Integrity:</label>
  <input id="baseHPInput" type="number" value="${baseHP}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="thresholdHPInput">Minimum Component Integrity required for functionality:</label>
  <input id="thresholdHPInput" type="number" value="${thresholdHP}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="overheatProbabilityInput">Overheat Probability:</label>
  <input id="overheatProbabilityInput" type="number" value="${overheatDamageProbability}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="overheatDamageInput">Overheat Damage per second:</label>
  <input id="overheatDamageInput" type="number" value="${overheatDamagePerSecond}">
</div>
<canvas id="chartCanvas"></canvas>
<script>
  const chartOption = {
    scales: {
      x: {
        title: {
          display: true,
          text: 'Battleshort Length (seconds)'
        }
      },
      y: {
        type: 'linear',
        display: true,
        position: 'right',
        title: {
          display: true,
          text: 'Battleshort Length Probability (%)'
        }
      },
      y1: {
        title: {
          display: true,
          text: 'Component Survival Probability (%)'
        },
        type: 'linear',
        display: true,
        position: 'left',
        grid: {
          drawOnChartArea: false
        },
      }
    }
  };
  const getData = () => {
    let baseHPData = Number(document.getElementById('baseHPInput').value);
    baseHPData = baseHPData > 0 ? baseHPData : 2;
    let thresholdHPData = Number(document.getElementById('thresholdHPInput').value);
    thresholdHPData = thresholdHPData > 0 ? thresholdHPData : 1;
    let overheatProbabilityData = Number(document.getElementById('overheatProbabilityInput').value);
    overheatProbabilityData = overheatProbabilityData > 0 ? overheatProbabilityData : 0.1;
    let overheatDamageData = Number(document.getElementById('overheatDamageInput').value);
    overheatDamageData = overheatDamageData > 0 ? overheatDamageData : 1;
    let data = [];
    let overheatAmountNeeded = Math.ceil((baseHPData - thresholdHPData) / overheatDamageData);
    let currAmount = overheatAmountNeeded;
    let pastProbability = 0;
    let currProbability = 0;
    let isIncreasing = true;
    while(isIncreasing || currProbability >= 0.001) {
      currProbability = jStat.negbin.pdf(currAmount - overheatAmountNeeded, overheatAmountNeeded, overheatProbabilityData);
      data.push({
        totalTime: currAmount,
        probability: currProbability,
        survivalProbability: jStat.binomial.cdf(overheatAmountNeeded, currAmount, overheatProbabilityData)
      });
      isIncreasing = pastProbability <= currProbability;
      pastProbability = currProbability;
      currAmount++;
    }
    return {
      labels: data.map(e => e.totalTime + 's'),
      datasets: [
        {
          label: 'Battleshort Length Probability',
          data: data.map(e => (e.probability * 100).toFixed(2)),
          borderColor: 'rgb(255, 99, 132)',
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
          fill: {
            target: 'start',
            below: 'rgba(255, 99, 132, 0.25)'
          }
        },
        {
          label: 'Component Survival Probability',
          data: data.map(e => (e.survivalProbability * 100).toFixed(2)),
          borderColor: 'rgb(54, 162, 235)',
          backgroundColor: 'rgba(54, 162, 235, 0.5)',
          fill: {
            target: 'start',
            below: 'rgba(54, 162, 235, 0.25)'
          },
          yAxisID: 'y1'
        }
      ]
    };
  }
  const config = {
    type: 'line',
    data: getData(),
    options: chartOption
  };
  let chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
  document.getElementById('baseHPInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('thresholdHPInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('overheatProbabilityInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('overheatDamageInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
</script>
[[/html]]
`;
}

exports.generateBurnthroughAmountProbabilityChart = (baseHP, thresholdHP, burnthroughDamageProbability, burnthroughDamage) => {
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div style="width: 100%; margin: 10px;">
  <label for="baseHPInput">Base Component Integrity:</label>
  <input id="baseHPInput" type="number" value="${baseHP}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="thresholdHPInput">Minimum Component Integrity required for functionality:</label>
  <input id="thresholdHPInput" type="number" value="${thresholdHP}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="burnthroughProbabilityInput">Overheat Probability:</label>
  <input id="burnthroughProbabilityInput" type="number" value="${burnthroughDamageProbability}">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="burnthroughDamageInput">Overheat Damage per second:</label>
  <input id="burnthroughDamageInput" type="number" value="${burnthroughDamage}">
</div>
<canvas id="chartCanvas"></canvas>
<script>
  const chartOption = {
    scales: {
      x: {
        title: {
          display: true,
          text: 'Burnthrough Amount'
        }
      },
      y: {
        title: {
          display: true,
          text: 'Burnthrough Amount Probability (%)'
        }
      },
      y1: {
        title: {
          display: true,
          text: 'Component Survival Probability (%)'
        },
        type: 'linear',
        display: true,
        position: 'left',
        grid: {
          drawOnChartArea: false
        },
      }
    }
  };
  const getData = () => {
    let baseHPData = Number(document.getElementById('baseHPInput').value);
    baseHPData = baseHPData > 0 ? baseHPData : 2;
    let thresholdHPData = Number(document.getElementById('thresholdHPInput').value);
    thresholdHPData = thresholdHPData > 0 ? thresholdHPData : 1;
    let burnthroughProbabilityData = Number(document.getElementById('burnthroughProbabilityInput').value);
    burnthroughProbabilityData = burnthroughProbabilityData > 0 ? burnthroughProbabilityData : 0.1;
    let burnthroughDamageData = Number(document.getElementById('burnthroughDamageInput').value);
    burnthroughDamageData = burnthroughDamageData > 0 ? burnthroughDamageData : 1;
    let data = [];
    let burnthroughAmountNeeded = Math.ceil((baseHPData - thresholdHPData) / burnthroughDamageData);
    let currAmount = burnthroughAmountNeeded;
    let pastProbability = 0;
    let currProbability = 0;
    let isIncreasing = true;
    while(isIncreasing || currProbability >= 0.001) {
      currProbability = jStat.negbin.pdf(currAmount - burnthroughAmountNeeded, burnthroughAmountNeeded, burnthroughProbabilityData);
      data.push({
        totalAmount: currAmount,
        probability: currProbability,
        survivalProbability: jStat.binomial.cdf(burnthroughAmountNeeded, currAmount, burnthroughProbabilityData)
      });
      isIncreasing = pastProbability <= currProbability;
      pastProbability = currProbability;
      currAmount++;
    }
    return {
      labels: data.map(e => e.totalAmount),
      datasets: [
        {
          label: 'Burnthrough Amount Probability',
          data: data.map(e => (e.probability * 100).toFixed(2)),
          borderColor: 'rgb(255, 99, 132)',
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
          fill: {
            target: 'start',
            below: 'rgba(255, 99, 132, 0.25)'
          }
        },
        {
          label: 'Component Survival Probability',
          data: data.map(e => (e.survivalProbability * 100).toFixed(2)),
          borderColor: 'rgb(54, 162, 235)',
          backgroundColor: 'rgba(54, 162, 235, 0.5)',
          fill: {
            target: 'start',
            below: 'rgba(54, 162, 235, 0.25)'
          },
          yAxisID: 'y1'
        }
      ]
    };
  }
  const config = {
    type: 'line',
    data: getData(),
    options: chartOption
  };
  let chart = new Chart(
    document.getElementById('chartCanvas'),
    config
  );
  document.getElementById('baseHPInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('thresholdHPInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('burnthroughProbabilityInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
  document.getElementById('burnthroughDamageInput').addEventListener('change', () => {
    chart.data = getData();
    chart.update();
  });
</script>
[[/html]]
`;
}

exports.generate3DRCSChart = (data, meshData = []) => {
  var LZUTF8 = require('lzutf8');
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/plotly.js/2.5.0/plotly.min.js" integrity="sha512-jMCLF90/C4/J/TNZtuowZn0erDL+1jpzdAklxIDs0O0Wr4qAHPt999DdN9WUNdYWpjfSOmLdBkltbP8Jki8mTg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sylvester/0.1.3/sylvester.min.js" integrity="sha512-5LqT6fHFrg0Ig5mLH37WNbbdhxpwZbk0q0jzQaKlzy8Tg2w0HRvPpS8OXPs4r85Yd6uGP5vcXrViwzs/OVS/9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lzutf8/0.6.3/lzutf8.min.js" integrity="sha512-jsnMjzAAVwJt3pKB5KXVMj/TO9pc8NrmEEde/ynXffVEEiaoH74/x2KIqIa0ZaLvNsJfNPaLdukkVWa0RRKbOw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div id="chartDiv"></div>
<script>
  let cdata = '${LZUTF8.compress(JSON.stringify(data), { outputEncoding: 'Base64' })}';
  let cmeshData = '${LZUTF8.compress(JSON.stringify(meshData), { outputEncoding: 'Base64' })}';
  let data = JSON.parse(LZUTF8.decompress(cdata, { inputEncoding: 'Base64' }));
  let meshData = JSON.parse(LZUTF8.decompress(cmeshData, { inputEncoding: 'Base64' }));
  let minRadar = jStat.min(data.map(e => e.value));
  let triangles = [];
  for(let i = 0;i < data.length;i++) {
    let currNeighbors = data[i].neighbors;
    if(currNeighbors[5] < 0) {
      currNeighbors.pop();
    }
    for(let j = 0;j < currNeighbors.length - 1;j++) {
      if(currNeighbors[j] >= 0 && currNeighbors[j + 1] >= 0) {
        triangles.push({
          i: i,
          j: currNeighbors[j],
          k: currNeighbors[j + 1]
        });
      }
    }
    if(currNeighbors[currNeighbors.length - 1] >= 0 && currNeighbors[0] >= 0) {
      triangles.push({
        i: i,
        j: currNeighbors[currNeighbors.length - 1],
        k: currNeighbors[0]
      });
    }
  }
  for(let i = 0;i < triangles.length;i++) {
    let currTriangle = triangles[i];
    let foundDuplicate = false;
    for(let j = 0;!foundDuplicate && j < i;j++) {
      let pastTriangle = triangles[j];
      if(
        (currTriangle.i === pastTriangle.i && currTriangle.j === pastTriangle.j && currTriangle.k === pastTriangle.k) ||
        (currTriangle.i === pastTriangle.i && currTriangle.j === pastTriangle.k && currTriangle.k === pastTriangle.j) ||
        (currTriangle.i === pastTriangle.j && currTriangle.j === pastTriangle.i && currTriangle.k === pastTriangle.k) ||
        (currTriangle.i === pastTriangle.j && currTriangle.j === pastTriangle.k && currTriangle.k === pastTriangle.i) ||
        (currTriangle.i === pastTriangle.k && currTriangle.j === pastTriangle.i && currTriangle.k === pastTriangle.j) ||
        (currTriangle.i === pastTriangle.k && currTriangle.j === pastTriangle.j && currTriangle.k === pastTriangle.i)
      ) {
        foundDuplicate = true;
      }
    }
    if(foundDuplicate) {
      triangles.splice(i,1);
      i--;
    }
  }
  let chartData = [
    {
      name: 'Radar Signature',
      type: 'mesh3d',
      x: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(e.value).e(1)),
      y: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(e.value).e(2)),
      z: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(e.value).e(3)),
      i: triangles.map(e => e.i),
      j: triangles.map(e => e.j),
      k: triangles.map(e => e.k),
      intensity: data.map(e => e.value),
      hovertext: data.map(e => 'RCS: ' + e.value + ' m^2'),
      colorscale: 'RdBu',
      opacity: 0.25
    }
  ];
  let largestMeshVertexMagnitude = 0;
  for(let mesh of meshData) {
    for(let i = 0;i < mesh.MeshVerticesData.length;i++) {
      let currVertex = mesh.MeshVerticesData[i];
      let mag = Vector.create([currVertex.x, currVertex.z, currVertex.y]).modulus();
      if(largestMeshVertexMagnitude < mag) {
        largestMeshVertexMagnitude = mag;
      }
    }
  }
  for(let mesh of meshData) {
    chartData.push({
      name: 'Ship' + Date.now(),
      type: 'mesh3d',
      x: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRadar / largestMeshVertexMagnitude).e(1)),
      y: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRadar / largestMeshVertexMagnitude).e(2)),
      z: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRadar / largestMeshVertexMagnitude).e(3)),
      i: mesh.MeshTriangleData.filter((e, i) => i % 3 === 0),
      j: mesh.MeshTriangleData.filter((e, i) => i % 3 === 1),
      k: mesh.MeshTriangleData.filter((e, i) => i % 3 === 2),
      color: 'rgb(0,0,0)'
    });
  }

  Plotly.newPlot('chartDiv', chartData, {
    width: 800,
    height: 800
  });
</script>
[[/html]]
`;
}

exports.generate3DDetectionChart = (data, meshData) => {
  var LZUTF8 = require('lzutf8');
  return `[[html]]
<script src="https://cdnjs.cloudflare.com/ajax/libs/plotly.js/2.5.0/plotly.min.js" integrity="sha512-jMCLF90/C4/J/TNZtuowZn0erDL+1jpzdAklxIDs0O0Wr4qAHPt999DdN9WUNdYWpjfSOmLdBkltbP8Jki8mTg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sylvester/0.1.3/sylvester.min.js" integrity="sha512-5LqT6fHFrg0Ig5mLH37WNbbdhxpwZbk0q0jzQaKlzy8Tg2w0HRvPpS8OXPs4r85Yd6uGP5vcXrViwzs/OVS/9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lzutf8/0.6.3/lzutf8.min.js" integrity="sha512-jsnMjzAAVwJt3pKB5KXVMj/TO9pc8NrmEEde/ynXffVEEiaoH74/x2KIqIa0ZaLvNsJfNPaLdukkVWa0RRKbOw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div style="width: 100%; margin: 10px;">
  <label for="radiatedPowerInput">Radiated Power (kW):</label>
  <input id="radiatedPowerInput" type="number" value="3500">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="burnthroughPowerMultiplierInput">Applied Burnthrough Power Multiplier:</label>
  <input id="burnthroughPowerMultiplierInput" type="number" value="1">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="gainInput">Gain (dB):</label>
  <input id="gainInput" type="number" value="40">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="sensitivityInput">Sensitivity (dBm):</label>
  <input id="sensitivityInput" type="number" value="-36">
</div>
<div style="width: 100%; margin: 10px;">
  <label for="apertureSizeInput">Aperture Size (m):</label>
  <input id="apertureSizeInput" type="number" value="25">
</div>
<div style="width: 100%; margin: 10px;">
  <button id="createChartButton">Create Chart</button>
</div>
<div id="chartDiv"></div>
<script>
  const detectionCalculation = (area) => {
    let radiatedPower = Number(document.getElementById('radiatedPowerInput').value);
    let burnthroughPowerMultiplier = Number(document.getElementById('burnthroughPowerMultiplierInput').value);
    let gain = Number(document.getElementById('gainInput').value);
    let sensitivity = Number(document.getElementById('sensitivityInput').value);
    let apertureSize = Number(document.getElementById('apertureSizeInput').value);

    let targetPowerDensity = Math.pow(10, sensitivity / 10) * 0.001;
    let realRadiatedPower = radiatedPower * burnthroughPowerMultiplier;
    let quadDistance = (realRadiatedPower * Math.pow(gain, 2) * (area / 10) * apertureSize) / (16 * Math.PI * Math.PI * targetPowerDensity);
    return Math.pow(quadDistance, 1/4);
  }

  let cdata = '${LZUTF8.compress(JSON.stringify(data), { outputEncoding: 'Base64' })}';
  let cmeshData = '${LZUTF8.compress(JSON.stringify(meshData), { outputEncoding: 'Base64' })}';
  let data = JSON.parse(LZUTF8.decompress(cdata, { inputEncoding: 'Base64' }));
  let meshData = JSON.parse(LZUTF8.decompress(cmeshData, { inputEncoding: 'Base64' }));
  let triangles = [];
  for(let i = 0;i < data.length;i++) {
    let currNeighbors = data[i].neighbors;
    if(currNeighbors[5] < 0) {
      currNeighbors.pop();
    }
    for(let j = 0;j < currNeighbors.length - 1;j++) {
      if(currNeighbors[j] >= 0 && currNeighbors[j + 1] >= 0) {
        triangles.push({
          i: i,
          j: currNeighbors[j],
          k: currNeighbors[j + 1]
        });
      }
    }
    if(currNeighbors[currNeighbors.length - 1] >= 0 && currNeighbors[0] >= 0) {
      triangles.push({
        i: i,
        j: currNeighbors[currNeighbors.length - 1],
        k: currNeighbors[0]
      });
    }
  }
  for(let i = 0;i < triangles.length;i++) {
    let currTriangle = triangles[i];
    let foundDuplicate = false;
    for(let j = 0;!foundDuplicate && j < i;j++) {
      let pastTriangle = triangles[j];
      if(
        (currTriangle.i === pastTriangle.i && currTriangle.j === pastTriangle.j && currTriangle.k === pastTriangle.k) ||
        (currTriangle.i === pastTriangle.i && currTriangle.j === pastTriangle.k && currTriangle.k === pastTriangle.j) ||
        (currTriangle.i === pastTriangle.j && currTriangle.j === pastTriangle.i && currTriangle.k === pastTriangle.k) ||
        (currTriangle.i === pastTriangle.j && currTriangle.j === pastTriangle.k && currTriangle.k === pastTriangle.i) ||
        (currTriangle.i === pastTriangle.k && currTriangle.j === pastTriangle.i && currTriangle.k === pastTriangle.j) ||
        (currTriangle.i === pastTriangle.k && currTriangle.j === pastTriangle.j && currTriangle.k === pastTriangle.i)
      ) {
        foundDuplicate = true;
      }
    }
    if(foundDuplicate) {
      triangles.splice(i,1);
      i--;
    }
  }

  const createChart = () => {
    let minRange = jStat.min(data.map(e => detectionCalculation(e.value)));
    let chartData = [
      {
        name: 'Detection Range',
        type: 'mesh3d',
        x: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(detectionCalculation(e.value)).e(1)),
        y: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(detectionCalculation(e.value)).e(2)),
        z: data.map(e => Vector.create([e.x, e.z, e.y]).multiply(detectionCalculation(e.value)).e(3)),
        i: triangles.map(e => e.i),
        j: triangles.map(e => e.j),
        k: triangles.map(e => e.k),
        intensity: data.map(e => detectionCalculation(e.value)),
        hovertext: data.map(e => 'Detection Range: ' + detectionCalculation(e.value) + ' m'),
        colorscale: 'RdBu',
        opacity: 0.25
      }
    ];
    let largestMeshVertexMagnitude = 0;
    for(let mesh of meshData) {
      for(let i = 0;i < mesh.MeshVerticesData.length;i++) {
        let currVertex = mesh.MeshVerticesData[i];
        let mag = Vector.create([currVertex.x, currVertex.z, currVertex.y]).modulus();
        if(largestMeshVertexMagnitude < mag) {
          largestMeshVertexMagnitude = mag;
        }
      }
    }
    for(let mesh of meshData) {
      chartData.push({
        name: 'Ship' + Date.now(),
        type: 'mesh3d',
        x: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRange / largestMeshVertexMagnitude).e(1)),
        y: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRange / largestMeshVertexMagnitude).e(2)),
        z: mesh.MeshVerticesData.map(e => Vector.create([e.x, e.z, e.y]).multiply(minRange / largestMeshVertexMagnitude).e(3)),
        i: mesh.MeshTriangleData.filter((e, i) => i % 3 === 0),
        j: mesh.MeshTriangleData.filter((e, i) => i % 3 === 1),
        k: mesh.MeshTriangleData.filter((e, i) => i % 3 === 2),
        color: 'rgb(0,0,0)'
      });
    }

    document.getElementById('chartDiv').innerHtml = '';
    Plotly.newPlot('chartDiv', chartData, {
      width: 800,
      height: 800
    });
  }

  document.getElementById('createChartButton').onclick = createChart;
  createChart();
</script>
[[/html]]
`;
}
