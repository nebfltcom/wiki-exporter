const fs = require('fs');
const utils = require('../utils');
const chart = require('./chart');

exports.getText = (data, version = '') => {
  let ret = '';
  ret += `[[div style="float: right; width: 30%;"]]
[[table style="width: 100%"]]
[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.ClassName)}_screenshot.png style="width: 95%;"]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #000000; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.ClassName)}_silhouette.png style="width: 95%;"]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**${data.ClassName} Class**
${data.HullClassification}
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Faction
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Faction === '' ? 'All' : data.Faction.replace(/Stock\//g,'')}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Size Class
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SizeClass}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Point Cost
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.PointCost}
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Maneuverability**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Mass
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Mass} Tonnes
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Speed
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MaxSpeed} m/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Turn Speed
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MaxTurnSpeed.toFixed(2)} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Linear Thrust
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.LinearMotor).toFixed(2)} MN
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Linear Acceleration
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${((data.LinearMotor * 1000000) / (data.Mass * 1000)).toFixed(2)} m/s^^2^^
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Time to Max Speed (Linear)
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.MaxSpeed / ((data.LinearMotor * 1000000) / (data.Mass * 1000))).toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Angular Thrust
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.AngularMotor).toFixed(2)} MN
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Durability**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Structural Integrity
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.BaseIntegrity}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor Thickness
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ArmorThickness} cm
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Interior Density Armor Equivalent
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.InteriorDensityArmorEquivalent.toFixed(2)} cm/m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Damage Resistance
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.MaxComponentDR * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Crew Vulnerability
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.CrewVulnerability * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Radar Signature
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
//See Chart//
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Identification Difficulty
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IdentityWorkValue}
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Other**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Base Crew Complement
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.BaseCrewComplement}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Visual Detection Distance
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.VisionDistance} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Hull Modifiers
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.EditorFormatHullBuffs ? utils.formatText(data.EditorFormatHullBuffs) : `//None//`}
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
//Data extracted from game version ${version}//
[[/div]]
[[/cell]]
[[/row]]
[[/table]]
[[/div]]

[[div style="float: left; width: 70%;"]]
[[div style="float: left; padding-right: 20px"]]
[[toc]]
[[/div]]

+ In-game Description

${data.LongDescription}

+ In-game Quote

> [[size 120%]]//@@ ${data.FlavorText.replace(/\n/g, '@@// \n> //@@ ')} @@//[[/size]]

+ Radar Signature 3D Chart

${chart.generate3DRCSChart(data.SignatureData, data.MeshData)}

This chart shows the 3D radar cross section diagram. Note that the data has been transformed so that the Y+ axis is forward and Z+ is up. Also note that the ship is not to scale and only used for orientation purposes.

+ Detection Range 3D Chart

${chart.generate3DDetectionChart(data.SignatureData, data.MeshData)}

This chart shows the 3D detection range diagram, which calculates the distance a simulated radar can detect the ship. Note that the data has been transformed so that the Y+ axis is forward and Z+ is up. Also note that the ship is not to scale and only used for orientation purposes.

[[/div]]
`;
  return ret;
}

exports.getList = (data, version = '') => {
  let ret = `[[table style="width: 100%"]]
[[row style="width: 100%"]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Class**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Type**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Point Cost**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Base Crew Complement**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Mass**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Max Speed**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Max Turn Speed**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Armor Thickness**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Structural Integrity**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Hull Modifiers**
[[/=]]
[[/cell]]
[[/row]]
`;
  for(let hull of data) {
    ret += `[[row style="width: 100%"]]
[[cell style="${utils.listTableBody}"]]
[[=]]
[[[/hull:${utils.formatName(hull.ClassName)}|${hull.ClassName} Class]]]
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.HullClassification}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.PointCost}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.BaseCrewComplement}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.Mass} Tonnes
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.MaxSpeed} m/s
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.MaxTurnSpeed.toFixed(2)} deg/s
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.ArmorThickness} cm
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.BaseIntegrity}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${hull.EditorFormatHullBuffs ? utils.formatText(hull.EditorFormatHullBuffs) : `//None//`}
[[/=]]
[[/cell]]
[[/row]]
`;
  }
  ret += `[[/table]]
//Data extracted from game version ${version}//
`;
  return ret;
}