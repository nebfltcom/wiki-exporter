const fs = require('fs');
const utils = require('../utils');
const chart = require('./chart');

exports.getText = (data, hasScreenshot = false, hasIcon = false, version = '') => {
  let ret = '';
  ret += `[[div style="float: right; width: 30%;"]]
[[table style="width: 100%"]]
`;

  if(hasScreenshot) {
    ret += `[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.MunitionName)}_screenshot.png style="width: 95%;"]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6;" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**${data.MunitionName}**
[[/div]]
[[/cell]]
[[/row]]
`;

  if(hasIcon) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Icon
[[/>]]
[[/cell]]
[[cell style="width: 60%; font-size: 110%; border: 1px solid grey; background-color: #000000; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.MunitionName)}_icon.png style="width: 95%;"]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Type}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Role
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Role}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Point Cost
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.PointCost} per ${data.PointDivision > 1 ? `${data.PointDivision} units` : `unit`}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Volume
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.StorageVolume} m^^3^^
[[/<]]
[[/cell]]
[[/row]]
`;

  if(data.MissileData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MissileData.Size}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Maneuverability**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Speed
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FlightSpeed} m/s
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Targeting**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MaxRange} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Position Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SupportsPositionTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Track Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SupportsTrackTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Visual Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SupportsVisualTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;

if(data.MissileData) {
  ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Guidance Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MissileData.Guidance}
[[/<]]
[[/cell]]
[[/row]]
`;
}

  if(data.DamageCharacteristics) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Damage**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor Penetration
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.ArmorPenetration} cm
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor Shredding Radius
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.DamageBrushSize.toFixed(2)} m
[[/<]]
[[/cell]]
[[/row]]
`;

    if(data.LWMunitionData && data.DamageCharacteristics.MaxPenetrationDepth >= 0) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Penetration
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.MaxPenetrationDepth} m
[[/<]]
[[/cell]]
[[/row]]
`;
    }

    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Overpenetration Damage Multiplier
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
x${data.DamageCharacteristics.OverpenetrationDamageMultiplier.toFixed(2)}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Damage
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.ComponentDamage} hp
[[/<]]
[[/cell]]
[[/row]]`;

if(data.PenetrationEnvelope)
{
ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
[[[mechanics:component-damage |Explosion Pendepth Envelope]]]
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.PenetrationEnvelope*100}%-100%
[[/<]]
[[/cell]]
[[/row]]`;
}

if(data.ExplosionRadius)
{
ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Explosion blast radius
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ExplosionRadius} m
[[/<]]
[[/cell]]
[[/row]]`;
}

ret +=`[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
[[[mechanics:damage-control |Critical Event Multiplier]]]
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.RandomEffectMultiplier.toFixed(2)}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
[[[mechanics:crew |Crew vulnerability Multiplier]]]
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.CrewVulnerabilityMultiplier.toFixed(2)}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Ignore effective Armor Thickness
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.IgnoreEffectiveThickness ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]`;

if(data.DamageCharacteristics.IgnoreEffectiveThickness == false) 
{ 
ret +=`[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Can Ricochet
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageCharacteristics.NeverRicochet ? utils.crossmark : utils.checkmark}
[[/<]]
[[/cell]]
[[/row]]`;
}
}

  ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
//Data extracted from game version ${version}//
[[/div]]
[[/cell]]
[[/row]]
[[/table]]
[[/div]]

[[div style="float: left; width: 70%;"]]
[[div style="float: left; padding-right: 20px"]]
[[toc]]
[[/div]]

+ In-game Description

${utils.formatMunitionDescription(data.Description)}
`;


//the whole '> [[size 120%]]//@@' and '@@// \n> //@@' thing is because @@ prevents formatting inside of it so it keeps the stylization of the text, but everything outside of it still applies the formatting thing
  if(data.FlavorText) 
  { 
ret +=`
+ In-game Quote
   
> [[size 120%]]//@@ ${data.FlavorText.replace(/\n/g, '@@// \n> //@@ ')} @@//[[/size]]
  `;
  }


  if(data.MissileData && data.MissileData.SignatureData) {
    ret += `
+ Radar Signature 3D Chart

${chart.generate3DRCSChart(data.MissileData.SignatureData)}

This chart shows the 3D radar cross section diagram. Note that the data has been transformed so that the Y+ axis is forward and Z+ is up.
`;
  }
/*
  if(data.ShellMunitionData) {
    ret += `
+ Penetration Falloff Chart

${chart.generatePenetrationFalloffChart(data.ShellMunitionData.PenetrationFalloffData, data.MaxRange)}

This chart shows the penetration falloff that occurs for a certain target range.
`;
  }
*/
  ret += `[[/div]]
`;
  return ret;
}

exports.getList = (data, version = '') => {
  let ret = `[[table style="width: 100%"]]
[[row style="width: 100%"]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Name**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Type**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Role**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Point Cost**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Volume**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Max Speed**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Max Range**
[[/=]]
[[/cell]]
[[/row]]
`;
  for(let munition of data) {
    ret += `[[row style="width: 100%"]]
[[cell style="${utils.listTableBody}"]]
[[=]]
[[[/munition:${utils.formatName(munition.MunitionName)}|${munition.MunitionName}]]]
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${munition.Type}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${munition.Role}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${munition.PointCost} per ${munition.PointDivision > 1 ? `${munition.PointDivision} units` : `unit`}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${munition.StorageVolume} m^^3^^
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${munition.FlightSpeed} m/s
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${typeof munition.MaxRange === 'number' ? munition.MaxRange + ' m' : 'N/A'}
[[/=]]
[[/cell]]
[[/row]]
`;
  }
  ret += `[[/table]]
//Data extracted from game version ${version}//
`;
  return ret;
}