const fs = require('fs');
const utils = require('../utils');
const chart = require('./chart');

exports.getText = (data, hasLargeImage = false, hasSmallImage = false, version = '') => {
  let ret = `[[div style="float: right; width: 30%;"]]
[[table style="width: 100%"]]
`;

  if(hasLargeImage) {
    ret += `[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.ComponentName)}_largeimage.png style="width: 95%;"]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row]]
[[cell style="width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6;" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**${data.ComponentName}**
[[/div]]
[[/cell]]
[[/row]]
`;

  if(hasSmallImage) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Icon
[[/>]]
[[/cell]]
[[cell style="width: 60%; font-size: 110%; border: 1px solid grey; background-color: #000000; padding: 10px;" colspan="2"]]
[[=image ${utils.formatName(data.ComponentName)}_smallimage.png style="width: 95%;"]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Category
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Category}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Type}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Faction
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Faction === '' ? 'All' : data.Faction.replace(/Stock\//g,'')}
[[/<]]
[[/cell]]
[[/row]]
`;

  if(data.ShortDescription.length > 0) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Short Description
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ShortDescription}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Cost**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Compounding Cost
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CompoundingCost ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;

  if(data.CompoundingCost) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Compounding Cost (Class)
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CompoundingCostClass}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Free First Instance
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FirstInstanceFree ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Point Cost
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.PointCost}
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Size**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Mass
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Mass} Tonnes
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Size.x}x${data.Size.y}x${data.Size.z}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Tiling
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CanTile ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Durability**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Integrity
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MaxHealth}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Reinforced
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.Reinforced ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Minimum Component Integrity required for functionality
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FunctioningThreshold}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Damage Threshold
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DamageThreshold}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Damage Control Priority
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCPriority}
[[/<]]
[[/cell]]
[[/row]]
`;

  if(data.RareDebuffSubtype.length > 0) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Rare Debuff Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RareDebuffSubtype}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Rare Debuff Chance
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.RareDebuffChance * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Resources**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Resources
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${utils.formatText(data.FormattedResources)}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Resource Demand Priority
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ResourceDemandPriority}
[[/<]]
[[/cell]]
[[/row]]
`;

  if(data.FormattedBuffs.length > 0) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Other**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Buffs
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${utils.formatText(data.FormattedBuffs)}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.IWeaponData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Weapon**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.WepType}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Optimal Size Class
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.OptimalTargetWeight}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
PDT Target Method
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.PDTTargetMethod}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Needs externally fed ammunition
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.NeedsExternalAmmoFeed ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(typeof data.IWeaponData.SupportsPositionTargeting === 'boolean') {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Position Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.SupportsPositionTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
    if(typeof data.IWeaponData.SupportsTrackTargeting === 'boolean') {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Track Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.SupportsTrackTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
    if(typeof data.IWeaponData.SupportsVisualTargeting === 'boolean') {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Supports Visual Targeting
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IWeaponData.SupportsVisualTargeting ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.WeaponComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Weapon Grouping Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.AllowGrouping ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.WeaponComponentData.AllowGrouping) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Weapon Grouping Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.GroupingName}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Weapon Role
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.Role}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
EW Role
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.EwType}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Checks for friendlies before firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.CheckFriendlyFire ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Checks for obstacles before firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.CheckObstaclesInWay ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Requires external ammunition feeding
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.RequireExternalAmmoFeed ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Allows ammunition selection
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.AllowAmmoSelection ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Will lead target
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.LeadTargets ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Required accuracy before firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.WeaponComponentData.OnTargetAngle}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.ContinuousWeaponComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burst Duration
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ContinuousWeaponComponentData.BurstDuration.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Cooldown Time
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ContinuousWeaponComponentData.CooldownTime.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Single Cycle per Order
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ContinuousWeaponComponentData.SingleCycleOrder ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Battleshort Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ContinuousWeaponComponentData.BattleshortAvailable ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;

    if(data.ContinuousWeaponComponentData.BattleshortAvailable) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Overheat Probability
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.ContinuousWeaponComponentData.OverheatDamageProbability * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Overheat Damage per Second
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ContinuousWeaponComponentData.OverheatDamagePerSecond}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Battleshort Length Probabilities
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
//See Chart//
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.DiscreteWeaponComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Time between firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DiscreteWeaponComponentData.TimeBetweenMuzzles.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Magazine Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DiscreteWeaponComponentData.MagazineSize}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Reload Time
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DiscreteWeaponComponentData.ReloadTime.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.DiscreteWeaponComponentData.TimeBetweenMuzzles > 0) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Fire rate (per load)
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(60 / data.DiscreteWeaponComponentData.TimeBetweenMuzzles).toFixed(2)} RPM
[[/<]]
[[/cell]]
[[/row]]
`;
    }
    if(data.DiscreteWeaponComponentData.TimeBetweenMuzzles > 0 || data.DiscreteWeaponComponentData.ReloadTime > 0) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Fire rate (sustained)
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(60/(data.DiscreteWeaponComponentData.TimeBetweenMuzzles * (data.DiscreteWeaponComponentData.MagazineSize - 1) + data.DiscreteWeaponComponentData.ReloadTime) * data.DiscreteWeaponComponentData.MagazineSize).toFixed(2)} RPM
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.CellLauncherComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Time between launches
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CellLauncherComponentData.TimeBetweenCells.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Launch Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(60 / data.CellLauncherComponentData.TimeBetweenCells).toFixed(2)} RPM
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Missile Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CellLauncherComponentData.MissileSize}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.TubeLauncherComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Time between launches
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TubeLauncherComponentData.TimeBetweenLaunches.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Missile Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TubeLauncherComponentData.MissileSize}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Magazine Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TubeLauncherComponentData.LaunchesPerLoad}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Reload Time
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TubeLauncherComponentData.LoadTime.toFixed(2)}s
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.FixedContinuousWeaponComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Steerable Beam
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FixedContinuousWeaponComponentData.SteerableBeam ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.FixedContinuousWeaponComponentData.SteerableBeam) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Steer Angle
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FixedContinuousWeaponComponentData.SteerAngle}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Steer Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FixedContinuousWeaponComponentData.SteerRate}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.MuzzleData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Muzzle**
[[/div]]
[[/cell]]
[[/row]]
`;
    if(data.MuzzleData.MaxRange) 
	{
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.MaxRange} m
[[/<]]
[[/cell]]
[[/row]]
`;
    }
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Simulation Method
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.SimMethod}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Ignore accuracy modifiers
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.IgnoreAccuracyModifiers ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Base Accuracy
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.BaseAccuracy}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.MuzzleData.AccuracyData) 
	{
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Shot Spread
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
//See Chart//
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.RaycastDamageCharacteristicData) 
  {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Direct Damage**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor Penetration
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.ArmorPenetration} cm
[[/<]]
[[/cell]]
[[/row]]
`;

		if(data.MuzzleData.armorPerSecond) 
		{
			ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor shred per second
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.armorPerSecond} cm/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Armor Shredding Radius
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.DamageBrushSize} m
[[/<]]
[[/cell]]
[[/row]]`;
		}
		if(data.RaycastDamageCharacteristicData.MaxPenetrationDepth) 
		{
			ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Maximum Penetration Depth
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.MaxPenetrationDepth} m
[[/<]]
[[/cell]]
[[/row]]`;
		}
		
ret +=`[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Overpenetration Damage Multiplier
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
x${data.RaycastDamageCharacteristicData.OverpenetrationDamageMultiplier}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Component Damage
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.ComponentDamage} hp
[[/<]]
[[/cell]]
[[/row]]`;
		
		
		if(data.MuzzleData.damagePerSecond) 
		{
			ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Damage per second
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.MuzzleData.damagePerSecond} hp/s
[[/<]]
[[/cell]]
[[/row]]`;
		}
		
		if(data.MuzzleData.damagePeriod) 
		{
			ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Hits per second
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${1/data.MuzzleData.damagePeriod}
[[/<]]
[[/cell]]
[[/row]]`;
		}		

ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
[[[mechanics:damage-control |Critical Event Multiplier]]]
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.RandomEffectMultiplier}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
[[[mechanics:crew |Crew vulnerability Multiplier]]]
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.CrewVulnerabilityMultiplier}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Ignore effective Armor Thicknesss
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.IgnoreAccuracyModifiers ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Can Ricochet
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.RaycastDamageCharacteristicData.NeverRicochet ? utils.crossmark : utils.checkmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Damage Falloff
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
//See Chart//
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.CommsAntennaComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Communications**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Transmit Power
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CommsAntennaComponentData.TransmitPower} kW
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Bandwidth
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CommsAntennaComponentData.Bandwidth} Bd
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Gain
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CommsAntennaComponentData.Gain} dB
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.OmnidirectionalEWarComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**EWAR**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Signature Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.OmnidirectionalEWarComponentData.SigType}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.OmnidirectionalEWarComponentData.MaxRange}m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Radiated Power
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.OmnidirectionalEWarComponentData.RadiatedPower} kW
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Gain
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.OmnidirectionalEWarComponentData.Gain} dB
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.TurretedEWarComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**EWAR**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedEWarComponentData.MaxRange} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Radiated Power
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedEWarComponentData.RadiatedPower} kW
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Gain
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedEWarComponentData.Gain} dB
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Field-of-View
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedEWarComponentData.ConeFov}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.ISensorComponentData || data.FireControlData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Sensor**
[[/div]]
[[/cell]]
[[/row]]
`;
  }

  if(data.ISensorComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Ignores EMCON
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ISensorComponentData.IgnoreEMCON ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.PassiveSensorComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Accuracy
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.PassiveSensorComponentData.Accuracy.toFixed(2)}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.SensorComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.MaxRange} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Radiated Power
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.RadiatedPower} kW
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Gain
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.Gain} dB
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Sensitivity
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.Sensitivity} dBm
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Aperture Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.ApertureSize} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Locking Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.CanLock ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;

    if(data.SensorComponentData.CanLock) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Signal-to-Noise ratio required to maintain lock
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.MaintainLockSNR}
[[/<]]
[[/cell]]
[[/row]]
`;
    }

    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burnthrough Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.ISensorComponentData.CanOrderBurnthrough ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;

    if(data.ISensorComponentData.CanOrderBurnthrough) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burnthrough Power Multiplier
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
x${data.SensorComponentData.BurnthroughPowerMultiplier}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burnthrough Damage Probability
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.SensorComponentData.BurnthroughDamageProbability * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burnthrough Damage
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorComponentData.BurnthroughDamage}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Burnthrough Amount Probability
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
//See Chart//
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.FireControlData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Range
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FireControlData.MaxRange} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Radiated Power
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FireControlData.RadiatedPower} kW
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Gain
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FireControlData.Gain} dB
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Aperture Size
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FireControlData.ApertureSize} m
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Locking Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${utils.checkmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Signal-to-Noise ratio required to maintain lock
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.FireControlData.MaintainLockSNR}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.IEWarTargetData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**EWAR Target**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Signature Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IEWarTargetData.SigType}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.SensorTurretComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Turret**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Traversal Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorTurretComponentData.TraverseRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Elevation Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorTurretComponentData.ElevationRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorTurretComponentData.MaxElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Min Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.SensorTurretComponentData.MinElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.TurretedContinuousWeaponComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Turret**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Traversal Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedContinuousWeaponComponentData.TraverseRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Elevation Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedContinuousWeaponComponentData.ElevationRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedContinuousWeaponComponentData.MaxElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Min Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedContinuousWeaponComponentData.MinElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Traversal Rate is limited while firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedContinuousWeaponComponentData.LimitRotationSpeedWhenFiring ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.TurretedContinuousWeaponComponentData.LimitRotationSpeedWhenFiring) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Traversal Rate Modifier while firing
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
x${data.TurretedContinuousWeaponComponentData.FiringRotationSpeedModifier}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.TurretedDiscreteWeaponComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Turret**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Traversal Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedDiscreteWeaponComponentData.TraverseRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Elevation Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedDiscreteWeaponComponentData.ElevationRate} deg/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Max Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedDiscreteWeaponComponentData.MaxElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Min Elevation
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.TurretedDiscreteWeaponComponentData.MinElevation}^^o^^
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.IIntelComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Intelligence**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Works on remote tracks
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IIntelComponentData.WorkOnRemoteTracks ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.CommandComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Intel Effort
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CommandComponentData.IntelEffort}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Intel Accuracy
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.CommandComponentData.IntelAccuracy * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.IntelligenceComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Intel Effort
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IntelligenceComponentData.IntelEffort}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Intel Accuracy
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${(data.IntelligenceComponentData.IntelAccuracy * 100).toFixed(2)}%
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.BerthingComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Crew**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Crew Provided
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.BerthingComponentData.CrewProvided}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.CrewOperatedComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Crew**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Crew Requirement
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CrewOperatedComponentData.CrewRequired}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Crew Reinforcement Threshold
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.CrewOperatedComponentData.ReinforceCrewAt < 0 ? `//Never//` : data.CrewOperatedComponentData.ReinforceCrewAt}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  if(data.DCLockerComponentData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Damage Control**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Teams Provided
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCLockerComponentData.TeamsProduced}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Repair Rate
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCLockerComponentData.TeamRepairRate.toFixed(2)} HP/sec
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Movement Speed
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCLockerComponentData.MovementSpeed.toFixed(2)} m/s
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Teams can restore components
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCLockerComponentData.CanRestore ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.DCLockerComponentData.CanRestore) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Restores Available
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.DCLockerComponentData.RestoreCount}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.IMagazineProviderData) {
    ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
**Magazine**
[[/div]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Volume Based
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IMagazineProviderData.VolumeBased ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Feeds Ammunition Externally
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IMagazineProviderData.CanFeedExternally ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Provides Ammunition
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IMagazineProviderData.CanProvide ? utils.checkmark : utils.crossmark}
[[/<]]
[[/cell]]
[[/row]]
`;
    if(data.IMagazineProviderData.CanProvide) {
      ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Provides Ammunition to Type
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.IMagazineProviderData.ProviderKey}
[[/<]]
[[/cell]]
[[/row]]
`;
    }
  }

  if(data.BulkMagazineComponentData) {
    ret += `[[row]]
[[cell style="${utils.sideTableLabel}"]]
[[>]]
Available Volume
[[/>]]
[[/cell]]
[[cell style="${utils.sideTableData}"]]
[[<]]
${data.BulkMagazineComponentData.AvailableVolume}
[[/<]]
[[/cell]]
[[/row]]
`;
  }

  ret += `[[row style="width: 100%; margin-top: 10px;"]]
[[cell style="${utils.sideTableHeader}" colspan="2"]]
[[div style="text-align: center; margin-bottom: -8px;"]]
//Data extracted from game version ${version}//
[[/div]]
[[/cell]]
[[/row]]
[[/table]]
[[/div]]

[[div style="float: left; width: 70%;"]]
[[div style="float: left; padding-right: 20px"]]
[[toc]]
[[/div]]

+ In-game Description

${data.LongDescription}

+ In-game Quote

> [[size 120%]]//@@ ${data.FlavorText.replace(/\n/g, '@@// \n> //@@ ')} @@//[[/size]]
`;

  if(data.SensorComponentData && Array.isArray(data.SensorComponentData.RadiationData)) {
    ret += `
+ Radiation Pattern Chart

${chart.generateRadiationPatternChart(data.SensorComponentData.RadiationData)}

This chart shows the radiation pattern of the sensor.Scale is from 0 to 100 and indicates the percentage of the max range that emission checks start to get conducted.
`;
  }

  if(data.FireControlData && Array.isArray(data.FireControlData.RadiationData)) {
    ret += `
+ Radiation Pattern Chart

${chart.generateRadiationPatternChart(data.FireControlData.RadiationData)}

This chart shows the radiation pattern of the fire control radar. Scale is from 0 to 100 and indicates the percentage of the max range that emission checks start to get conducted.
`;
  }

  if(data.MuzzleData && data.MuzzleData.AccuracyData) {
    ret += `
+ Shot Spread Chart

${chart.generateAccuracyChart(data.MuzzleData.AccuracyData, data.MuzzleData.MaxRange)}

This chart shows the shot spread diameter at different target ranges.
`;
  }

  if(data.RaycastDamageFalloffData) {
    ret += `
+ Damage Falloff Chart

${chart.generateDamageFalloffChart(data.RaycastDamageCharacteristicData.ComponentDamage, data.RaycastDamageFalloffData)}

This chart shows the damage falloff that occurs for a certain target range.
`;
  }

  if(data.ContinuousWeaponComponentData && data.ContinuousWeaponComponentData.BattleshortAvailable) {
    ret += `
+ Battleshort Length Probability Chart

${chart.generateBattleshortLengthProbabilityChart(data.MaxHealth, data.FunctioningThreshold, data.ContinuousWeaponComponentData.OverheatDamageProbability, data.ContinuousWeaponComponentData.OverheatDamagePerSecond)}

This chart shows the probability distribution (in red) of the amount of time that a fully functioning component can be battleshorted for. It also shows the probability distribution (in blue) of the likelyhood of a component survives being battleshorted for that long. This does not account for repairs or other sources of damage. Original burst duration isn't added either, battleshort length is defined as firing beyond the base firing duration.
`;
  }

  if(data.ISensorComponentData && data.ISensorComponentData.CanOrderBurnthrough) {
    ret += `
+ Burnthrough Amount Probability Chart

${chart.generateBurnthroughAmountProbabilityChart(data.MaxHealth, data.FunctioningThreshold, data.SensorComponentData.BurnthroughDamageProbability, data.SensorComponentData.BurnthroughDamage)}

This chart shows the probability distribution of the amount of burnthroughs that a fully functioning component can withstand. This does not account for repairs or other sources of damage.
`;
  }

  ret += `[[/div]]
`;
  return ret;
}

exports.getList = (data, version = '') => {
  let ret = `[[table style="width: 100%"]]
[[row style="width: 100%"]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Name**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Category**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Type**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Point Cost**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Mass**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Component Integrity**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Reinforced**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Component Damage Threshold**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Size**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Tiling**
[[/=]]
[[/cell]]
[[cell style="${utils.listTableHeader}"]]
[[=]]
**Component Buffs**
[[/=]]
[[/cell]]
[[/row]]
`;
  for(let component of data) {
    ret += `[[row style="width: 100%"]]
[[cell style="${utils.listTableBody}"]]
[[=]]
[[[/component:${utils.formatName(component.ComponentName)}|${component.ComponentName}]]]
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.Category}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.Type}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.PointCost}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.Mass} Tonnes
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.MaxHealth}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.Reinforced ? utils.checkmark : utils.crossmark}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.DamageThreshold}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.Size.x}x${component.Size.y}x${component.Size.z}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.CanTile ? utils.checkmark : utils.crossmark}
[[/=]]
[[/cell]]
[[cell style="${utils.listTableBody}"]]
[[=]]
${component.FormattedBuffs.length > 0 ? utils.formatText(component.FormattedBuffs) : `//None//`}
[[/=]]
[[/cell]]
[[/row]]
`;
  }
  ret += `[[/table]]
//Data extracted from game version ${version}//
`;
  return ret;
}