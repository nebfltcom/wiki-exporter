const path = require('path');
const fs = require('fs');
const puppet = require('./wiki/puppet');
const hull = require('./wiki/hull');
const component = require('./wiki/component');
const munition = require('./wiki/munition');
const utils = require('./utils');

const delay = async (timeout) => {
  return new Promise((r) => {
    setTimeout(r, timeout)
  });
}

let list = JSON.parse(fs.readFileSync(path.resolve('./tmp/data/index.json')));
let data = {
  hulls: [],
  components: [],
  munitions: []
};
if(!fs.existsSync(path.resolve('./tmp/data/finished.json'))) {
  fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify({
    hulls: [],
    components: [],
    munitions: [],
    hullList: false,
    componentList: false,
    munitionList: false
  }));
}
let finishedList = JSON.parse(fs.readFileSync(path.resolve('./tmp/data/finished.json')));
for(let listHull of list.hulls) {
  let hullData = JSON.parse(fs.readFileSync(path.resolve(`./tmp/data/${listHull}`)));
  data.hulls.push(hullData);
}
data.hulls.sort((h1, h2) => h1.Mass - h2.Mass);
for(let listComponent of list.components) {
  let componentData = JSON.parse(fs.readFileSync(path.resolve(`./tmp/data/${listComponent}`)));
  data.components.push(componentData);
}
data.components.sort((h1, h2) => {
  if(h1.Category !== h2.Category) {
    if(h1.Category < h2.Category) {
      return -1;
    }
    else {
      return 1;
    }
  }
  if(h1.Type !== h2.Type) {
    if(h1.Type < h2.Type) {
      return -1;
    }
    else {
      return 1;
    }
  }
  if(h1.ComponentName !== h2.ComponentName) {
    if(h1.ComponentName < h2.ComponentName) {
      return -1;
    }
    else {
      return 1;
    }
  }
  return h1.PointCost - h2.PointCost;
});
for(let listMunition of list.munitions) {
  let munitionData = JSON.parse(fs.readFileSync(path.resolve(`./tmp/data/${listMunition}`)));
  data.munitions.push(munitionData);
}
data.munitions.sort((h1, h2) => {
  if(h1.Type !== h2.Type) {
    if(h1.Type < h2.Type) {
      return -1;
    }
    else {
      return 1;
    }
  }
  if(h1.MunitionName !== h2.MunitionName) {
    if(h1.MunitionName < h2.MunitionName) {
      return -1;
    }
    else {
      return 1;
    }
  }
  return h1.PointCost - h2.PointCost;
});

(async () => {
  await puppet.init();
  for(let hullData of data.hulls) {
    if(!finishedList.hulls.includes(utils.formatName(hullData.ClassName))) {
      let screenshotPath = path.resolve(`./tmp/images/${utils.formatName(hullData.ClassName)}_screenshot.png`);
      let hasScreenshot = fs.existsSync(screenshotPath);
      if(hasScreenshot) {
        await puppet.upload(`hull:${utils.formatName(hullData.ClassName)}`, screenshotPath);
        await delay(3000);
      }
      let silhouettePath = path.resolve(`./tmp/images/${utils.formatName(hullData.ClassName)}_silhouette.png`);
      let hasSilhouette = fs.existsSync(silhouettePath);
      if(hasSilhouette) {
        await puppet.upload(`hull:${utils.formatName(hullData.ClassName)}`, silhouettePath);
        await delay(3000);
      }
      await puppet.edit(`hull:${utils.formatName(hullData.ClassName)}`, `${hullData.ClassName} Class ${hullData.HullClassification}`, hull.getText(hullData, list.version));
      await delay(3000);

      finishedList.hulls.push(utils.formatName(hullData.ClassName));
      fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
    }
  }
  if(!finishedList.hullList) {
    await puppet.edit('hull:list', 'List of Hulls', hull.getList(data.hulls, list.version));
    await delay(3000);
    
    finishedList.hullList = true;
    fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
  }

  for(let componentData of data.components) {
    if(!finishedList.components.includes(utils.formatName(componentData.ComponentName))) {
      let largeImagePath = path.resolve(`./tmp/images/${utils.formatName(componentData.ComponentName)}_largeimage.png`);
      let hasLargeImage = fs.existsSync(largeImagePath);
      if(hasLargeImage) {
        await puppet.upload(`component:${utils.formatName(componentData.ComponentName)}`, largeImagePath);
        await delay(3000);
      }
      let smallImagePath = path.resolve(`./tmp/images/${utils.formatName(componentData.ComponentName)}_smallimage.png`);
      let hasSmallImage = fs.existsSync(smallImagePath);
      if(hasSmallImage) {
        await puppet.upload(`component:${utils.formatName(componentData.ComponentName)}`, smallImagePath);
        await delay(3000);
      }
      await puppet.edit(`component:${utils.formatName(componentData.ComponentName)}`, `${componentData.ComponentName}`, component.getText(componentData, hasLargeImage, hasSmallImage, list.version));
      await delay(3000);

      finishedList.components.push(utils.formatName(componentData.ComponentName));
      fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
    }
  }
  if(!finishedList.componentList) {
    await puppet.edit('component:list', 'List of Components', component.getList(data.components, list.version));
    await delay(3000);
    
    finishedList.componentList = true;
    fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
  }
  for(let munitionData of data.munitions) {
    if(!finishedList.munitions.includes(utils.formatName(munitionData.MunitionName))) {
      let screenshotPath = path.resolve(`./tmp/images/${utils.formatName(munitionData.MunitionName)}_screenshot.png`);
      let hasScreenshot = fs.existsSync(screenshotPath);
      if(hasScreenshot) {
        await puppet.upload(`munition:${utils.formatName(munitionData.MunitionName)}`, screenshotPath);
        await delay(3000);
      }
      let iconPath = path.resolve(`./tmp/images/${utils.formatName(munitionData.MunitionName)}_icon.png`);
      let hasIcon = fs.existsSync(iconPath);
      if(hasIcon) {
        await puppet.upload(`munition:${utils.formatName(munitionData.MunitionName)}`, iconPath);
        await delay(3000);
      }
      await puppet.edit(`munition:${utils.formatName(munitionData.MunitionName)}`, `${munitionData.MunitionName}`, munition.getText(munitionData, hasScreenshot, hasIcon, list.version));
      await delay(3000);

      finishedList.munitions.push(utils.formatName(munitionData.MunitionName));
      fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
    }
  }
  if(!finishedList.munitionList) {
    await puppet.edit('munition:list', 'List of Munitions', munition.getList(data.munitions, list.version));
    await delay(3000);
    
    finishedList.munitionList = true;
    fs.writeFileSync(path.resolve('./tmp/data/finished.json'), JSON.stringify(finishedList, null, 2));
  }
  process.exit();
})();