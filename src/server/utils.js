const Color = require('color');

exports.formatName = (name) => {
  return name.replace(/ /g, '_').replace(/['".]/g, '').toLowerCase();
}

exports.formatText = (text) => {
  return text.replace(/<color=#([\w\d]+)>/g, (match, p1) => {
    let c = Color(`#${p1}`);
    if(c.isLight()) {
      c = c.darken(0.5);
    }
    return `#${c.hex()}|`;
  }).replace(/<\/color>/g,'##');
}

exports.formatMunitionDescription = (text) => {
  if(text.indexOf('Size:')) {
    return text.substr(0, text.indexOf('\nSize:'));
  }
  if(text.indexOf('Type:')) {
    return text.substr(0, text.indexOf('\nType:'));
  }
}

exports.checkmark = '✔️';
exports.crossmark = '❌';

exports.sideTableHeader = `width: 100%; font-size: 110%; border: 1px solid grey; background-color: #F6F9F6;`;
exports.sideTableLabel = `width: 40%; font-size: 110%; border: 1px solid grey; background-color: #E6E9E6; padding-right: 3px;`;
exports.sideTableData = `width: 60%; font-size: 110%; border: 1px solid grey; background-color: #FFFFFF; padding-left: 3px;`;

exports.listTableHeader = `font-size: 110%; border: 1px solid grey; background-color: #E6E9E6;`;
exports.listTableBody = `font-size: 110%; border: 1px solid grey; background-color: #F6F9F6;`;
