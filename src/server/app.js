const path = require('path');
const fs = require('fs');
const child_process = require('child_process');
const utils = require('./utils');
const check = require('./check');
const express = require('express');
const app = express();
const port = 9997;

//Clear tmp directory
console.info('[Server] Clearing tmp folders');
try { fs.rmSync(path.resolve('tmp/images'), { recursive: true, force: true }); } catch(err) {}
try { fs.rmSync(path.resolve('tmp/spellcheck'), { recursive: true, force: true }); } catch(err) {}
try { fs.rmSync(path.resolve('tmp/data'), { recursive: true, force: true }); } catch(err) {}
try { fs.mkdirSync(path.resolve('tmp')); } catch(err) {}
try { fs.mkdirSync(path.resolve('tmp/images')); } catch(err) {}
try { fs.mkdirSync(path.resolve('tmp/spellcheck')); } catch(err) {}
try { fs.mkdirSync(path.resolve('tmp/data')); } catch(err) {}
try { fs.rmSync(path.resolve('./tmp/gamedir/Mods'), { recursive: true, force: true }); } catch(err) {}
try { fs.rmSync(path.resolve('./tmp/gamedir/Saves'), { recursive: true, force: true }); } catch(err) {}

let list = {
  version: '',
  hulls: [],
  components: [],
  munitions: []
};

let gameProcess = child_process.spawn('Nebulous.exe', ['-screen-fullscreen', '0', '-screen-height', '480', '-screen-width', '640'], {
  cwd: path.resolve('./tmp/gamedir')
});

app.use(express.json({
  limit: '1gb'
}));

app.get('/filepath', (req, res) => {
  console.info('[Server] Tmp folder path requested');
  res.send(path.resolve('./tmp/images'));
});

app.put('/version', (req, res) => {
  let data = req.body;
  console.info(`[Server] Received game version`);
  list.version = data.Version;
});

app.put('/hull', (req, res) => {
  let data = req.body;
  console.info(`[Server] Received Hull ${data.ClassName} data`);
  check.checkData(data, path.resolve('./tmp/spellcheck'));
  fs.writeFileSync(path.resolve('./tmp/data/' + utils.formatName(data.ClassName) + '.json'), JSON.stringify(data, null, 2));
  list.hulls.push(utils.formatName(data.ClassName) + '.json');
});

app.put('/component', (req, res) => {
  let data = req.body;
  console.info(`[Server] Received Component ${data.ComponentName} data`);
  check.checkData(data, path.resolve('./tmp/spellcheck'));
  fs.writeFileSync(path.resolve('./tmp/data/' + utils.formatName(data.ComponentName) + '.json'), JSON.stringify(data, null, 2));
  list.components.push(utils.formatName(data.ComponentName) + '.json');
});

app.put('/munition', (req, res) => {
  let data = req.body;
  console.info(`[Server] Received Munition ${data.MunitionName} data`);
  check.checkData(data, path.resolve('./tmp/spellcheck'));
  fs.writeFileSync(path.resolve('./tmp/data/' + utils.formatName(data.MunitionName) + '.json'), JSON.stringify(data, null, 2));
  list.munitions.push(utils.formatName(data.MunitionName) + '.json');
});

app.put('/end', (req, res) => {
  console.info('[Server] Received end signal');
  fs.writeFileSync(path.resolve('./tmp/data/index.json'), JSON.stringify(list, null, 2));
  process.exit();
})

app.listen(port, () => {
  console.info(`[Server] NEBFLTCOM Wiki Exporter is listening on port ${port}`);
});

process.on('exit', () => {
  gameProcess.kill('SIGKILL');
});