# Wiki Exporter

NEBULOUS: Fleet Command BepinEx plugin for exporting to nebfltcom wiki.

This software can be used to extract useful in game data. This data is then processed and formatted for the site http://nebfltcom.wikidot.com.

# Usage

## Prerequisite

1) .NET SDK 6.0.200
    - https://dotnet.microsoft.com/en-us/download/visual-studio-sdks.
2) Node.js v16.13.2 and NPM 8.1.2
    - https://nodejs.org/en/download/

*Note: Exact versions are not strictly required and older versions may work. These version numbers are provided for replicating known working setup.*

## Automatic Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/wiki-exporter/-/archive/main/wiki-exporter-main.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file.
3) Run the command `npm install`.
4) Run the command `npm run setup`.

## Manual Installation

1) Copy the game into another directory (this will be the modded game directory). The instructions below will refer to this new directory as `<game directory>`.
2) Remove the `<game directory>\Mods` folder if it exists.
3) Download BepInEx 5.4.19 x64 (https://github.com/BepInEx/BepInEx/releases/download/v5.4.19/BepInEx_x64_5.4.19.0.zip).
4) Extract the BepInEx zip contents into `<game directory>`, overwriting files as needed.
5) Run `<game directory>\Nebulous.exe`, verifying `<game directory>\BepInEx\plugins` gets created. Quit the game when this folder is created.
6) *(Optional)* Edit `<game directory>\BepInEx\config\BepInEx.cfg` and replace line 48 with `Enabled = true`
7) Download or clone this repository if needed. The instructions below will refer to repository path as `<exporter directory>`.
8) Copy all `.dll` files in `<game directory>\Nebulous_Data\Managed` to `<exporter directory>\src\plugin\lib`.
9) Open up a command prompt or powershell session and navigate to `<exporter directory>`.
10) Run the command `npm install`.
11) In the command prompt or powershell session, navigate to `<exporter directory>\src\plugin`.
12) Run the command `dotnet build`.
13) Copy these files from `<exporter directory>\src\plugin\bin\Debug\net46` to `<game directory>\BepInEx\plugins`.
    - `Newtonsoft.Json.dll`
    - `UniverseLib.Mono.dll`
    - `wikiexporter.dll`
14) Copy `C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\v3.0\System.Runtime.Serialization.dll` to `<game directory>\BepInEx\plugins`.

## Usage

### Extracting Data

This process will remove any previously extracted data that exists in `<exporter directory>\tmp`.

1) Open up a command prompt or powershell session and navigate to `<exporter directory>`.
2) Run the command `npm run server`.
3) Wait for the program to exit.

Extracted data will show up in `<exporter directory>\tmp` as json and image files.

### Uploading Data

This process will upload image files and formatted pages to a wikidot site.

1) If `<exporter directory>\config.json` does not exist, create a copy of `<exporter directory>\config.default.json` and rename it as `<exporter directory>\config.json`.
2) Double check `<exporter directory>\config.json` settings are correct.
3) Open up a command prompt or powershell session and navigate to `<exporter directory>`.
4) Run the command `npm run wiki`.
5) Wait for the program to exit, which can take longer than an hour. If the program exits with a bunch of errors, rerun the command `npm run wiki` to retry and continue uploading.