**Note: This file may be outdated, look for true json schema under src/plugin/JsonClasses**

# Hull

 - [`string`] ClassName
 - [`string`] HullClassification
 - [`Texture2D`] Silhouette.texture
 - [`Texture2D`] HullScreenshot.texture
 - [`string`] LongDescription
 - [`string`] FlavorText
 - [`int`] PointCost
 - [`string`] SizeCost.ToString()
 - [`int`] BaseCrewComplement
 - [`float`] Mass
 - [`float`] MaxSpeed => MaxSpeed * 10
 - [`float`] MaxTurnSpeed => MaxTurnSpeed * 57.29578
 - [`float`] LinearMotor
 - [`float`] AngularMotor
 - [`float`] StatSigmultRadar.BaseValue => StatSigmultRadar.BaseValue * 10
 - [`float`] CrewVulnerability
 - [`float`] ArmorThickness
 - [`float`] MaxComponentDR
 - [`int`] VisionDistance => VisionDistance * 500
 - [`float`] _structure._baseIntegrity
 - [`float`] IdentityWorkValue
 - [`string`] EditorFormatHullBuffs()

# HullComponent

 - [`string`] ComponentName
 - [`Texture2D`] SmallImage.texture
 - [`Texture2D`] LargeImage.texture
 - [`string`] _longDescription
 - [`string`] FlavorText
 - [`string`] Category
 - [`bool`] CompoundingCost
 - [`float`] _compoundingMultiplier
 - [`string`] CompoundingCostClass
 - [`bool`] FirstInstanceFree
 - [`int`] PointCost
 - [`string`] Type.ToString()
 - [`Vector3Int] Size
 - [`float`] Mass
 - [`bool`] CanTile
 - [`string`] DCPriority.ToString()
 - [`string`] GetFormattedResources()
 - [`string`] ResourceDemandPriority.ToString()
 - [`string`] GetFormattedBuffs()
 - [`float`] _maxHealth
 - [`float`] _functioningThreshold
 - [`float`] _damageThreshold
 - [`bool`] _reinforced
 - [`float`] _rareDebuffChance
 - [`string`] _rareDebuffSubtype
 - [`AnimationCurve`] _debuffProbability

# BerthingComponent (inherits CrewedComponent)

 - [`int`] _crewProvided

# BulkMagazineComponent (inherits IMagazineProvider)

 - [`int`] _availableVolume

# CellLauncherComponent (inherits IWeapon, IMagazineProvider)

 - [`float`] _timeBetweenCells
 - [`int`] _missileSize
 - [`int`] Capacity
 - ~~[`bool`] SupportsPositionTargeting~~
 - ~~[`bool`] PositionTargetingNoInput~~
 - ~~[`bool`] SupportsTrackTargeting~~
 - ~~[`bool`] SupportsVisualTargeting~~
 - ~~[`bool`] SupportsTargetReferencePoints~~

# CommandComponent (inherits CrewOperatedComponent, IIntelComponent)

 - [`float`] _intelEffort
 - [`float`] _intelAccuracy

# CommsAntennaComponent (inherits ICommsAntenna)

 - [`float`] _transmitPower
 - [`int`] _bandwidth
 - [`float`] _gain

# ContinuousWeaponComponent (inherits WeaponComponent)

 - [`float`] _burstDuration
 - [`float`] _cooldownTime
 - [`float`] _gradualCooldownSpeed
 - [`float`] _overheatDamageProbability
 - [`float`] _overheatDamagePerSecond
 - [`bool`] _singleCycleOrder
 - [`float`] MaxEffectiveRange
 - [`string`] _cooldownStyle.ToString()

# CrewOperatedComponent (inherits CrewedComponent)

 - [`int`] _crewRequired
 - [`int`] _reinforceCrewAt

# DCLockerComponent (inherits CrewOperatedComponent)

 - [`int`] TeamsProduced
 - [`float`] TeamRepairRate
 - [`float`] _movementSpeed
 - [`bool`] _canRestore
 - [`int`] _restoreCount

# DiscreteWeaponComponent (inherits WeaponComponent)

 - [`float`] _timeBetweenMuzzles
 - [`int`] _magazineSize
 - [`float`] _reloadTime

# FixedContinuousWeaponComponent (inherits ContinuousWeaponComponent)

 - [`bool`] _steerableBeam
 - [`float`] _steerAngle
 - [`float`] _steerRate

# IntelligenceComponent (inherits CrewOperatedComponent, IIntelComponent)

 - [`float`] _intelEffort
 - [`float`] _intelAccuracy

# OmnidirectionalEWarComponent (inherits ContinuousWeaponComponent)

 - [`string`] _sigType.ToString()
 - [`float`] _maxRange
 - [`float`] _effectAreaRatio
 - [`float`] _radiatedPower
 - [`float`] _gain

# PassiveSensorComponent (inherits ISensorComponent, ISensor, IEWarTarget)

 - [`float`] _accuracy

# SensorComponent (inherits ISensorComponent, ISensor, IEWarTarget)

 - [`float`] _maxRange
 - [`float`] _radiatedPower
 - [`float`] _gain
 - [`float`] _sensitivity
 - [`float`] _apertureSize
 - [`AnimationCurve`] _deviationCurve
 - [`bool`] _canLock
 - [`float`] _lockErrorMultiplier
 - [`float`] _maintainLockSNR
 - [`float`] _burnthroughPowerMultiplier
 - [`float`] _burnthroughDamageProbability
 - [`float`] _burnthroughDamage

# SensorTurretComponent (inherits ISensorComponent, ISensor, IEWarTarget)

 - [`float`] _traverseRate
 - [`float`] _elevationRate
 - [`float`] _minElevation
 - [`float`] _maxElevation

# TubeLauncherComponent (inherits CycledComponent, IWeapon)

 - [`int`] _missileSize
 - [`float`] _loadTime
 - [`int`] _launchesPerLoad
 - [`float`] _timeBetweenLaunches

# TurretedContinuousWeaponComponent (inherits ContinuousWeaponComponent)

 - [`float`] _traverseRate
 - [`float`] _elevationRate
 - [`bool`] _limitRotationSpeedWhenFiring
 - [`float`] _firingRotationSpeedModifier
 - [`float`] _minElevation
 - [`float`] _maxElevation

# TurretedDiscreteWeaponComponent (inherits DiscreteWeaponComponent)

 - [`float`] _traverseRate
 - [`float`] _elevationRate
 - [`float`] _minElevation
 - [`float`] _maxElevation

# TurretedEWarComponent (inherits TurretedContinuousWeaponComponent)

 - [`float`] _coneFov
 - [`float`] _maxRange
 - [`float`] _effectAreaRatio
 - [`float`] _radiatedPower
 - [`float`] _gain

# WeaponComponent (inherits CycledComponent, IWeapon)

 - [`string`] _optimalTargetWeight.ToString() <WeightClass>
 - [`bool`] _allowGrouping
 - [`string`] _groupingName
 - [`bool`] _isPointDefense
 - [`bool`] _isEW
 - [`bool`] _checkFriendlyFire
 - [`bool`] _checkObstaclesInWay
 - [`bool`] _requireExternalAmmoFeed
 - [`bool`] _allowAmmoSelection
 - [`bool`] _leadTargets
 - [`float`] _onTargetAngle

# IEWarTarget

 - [`string`] _sigType.ToString()

# IIntelComponent

 - [`bool`] WorkOnRemoteTracks

# IMagazineProvider

 - [`bool`] VolumeBased
 - [`bool`] CanFeedExternally
 - [`bool`] CanProvide
 - [`string`] ProviderKey

# ISensorComponent

 - [`bool`] IgnoreEMCON
 - [`bool`] CanOrderLock
 - [`bool`] CanOrderBurnthrough

# IWeapon

 - [`string`] WepType.ToString()
 - [`string`] OptimalTargetWeight.ToString() <WeightClass>
 - [`string`] PDTTargetMethod.ToString() <TargetMethod>
 - [`bool`] NeedsExternalAmmoFeed
 - [`bool`] SupportsAmmoSelection
 - [`bool`] SupportsPositionTargeting
 - [`bool`] PositionTargetingNoInput
 - [`bool`] SupportsTrackTargeting
 - [`bool`] SupportsVisualTargeting
 - [`bool`] SupportsTargetReferencePoints

# IMunition

 - [`string`] MunitionName
 - [`Texture2D`] DetailScreenshot.texture
 - [`string`] Type.ToString() <MunitionType>
 - [`string`] SimMethod.ToString() <MunitionSimulationMethod>
 - [`string`] Role.ToString() <WeaponRole>
 - [`int`] PointCost
 - [`int`] PointDivision
 - [`float`] StorageVolume
 - [`bool`] SupportsPositionTargeting
 - [`bool`] PositionTargetingNoInput
 - [`bool`] SupportsTrackTargeting
 - [`bool`] SupportsVisualTargeting
 - [`bool`] SupportsTargetReferencePoints
 - [`float`] FlightSpeed => FlightSpeed * 10
 - [`float`] TrackBackoff
 - [`float`] Lifetime
 - [`float`] MaxRange
 - [`IDamageCharacteristic`] DamageCharacteristics
 - [`string`] GetDescription()
 - [`string`] GetDetailText()

# IDamageCharacteristic

 - [`float`] ArmorPenetration
 - [`float`] OverpenetrationDamageMultiplier
 - [`float`] ComponentSearchDistance
 - [`float`] HeatDamage
 - [`float`] DamageBrushSize
 - [`float`] ComponentDamage
 - [`float`] RandomEffectMultiplier
 - [`float`] CrewVulnerabilityMultiplier
 - [`bool`] IgnoreEffectiveThickness
 - [`bool`] NeverRicochet
